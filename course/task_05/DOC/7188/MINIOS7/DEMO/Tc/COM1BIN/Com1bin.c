/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: Com1bin.prj
   	    Com1bin.c
	    i7188s.lib
4. Explain: Test the 7527.

5. Hordware: 7188
It's compiled by Tony
----------------------------------
*/

#include"..\lib\i7188.h"
/*--------------------------------*/

void main(void)
{
  int quit=0;
  int data;
  int type;
  int ver;
  type=Is7188();		/*detect the current operatin system*/
   if(type)
     {
     ver=GetLibVersion();
     Print("Hello 7188! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
     }
   else
     {
     Print("Hello PC!, this program is not run under 7188.");
     return;
     }

InstallCom1(115200L,8,0,1);
for (;;)
    {
    if (IsCom1())
       {
       data=ReadCom1();
       switch(data)
	   {
	   case  0  : ToCom1(1);
		      ToCom1(0); ToCom1(1); ToCom1(0);
		      break;
	   case  2  :
		      while (!IsCom1());
		      data=ReadCom1(); if (data!=3) goto ret_label;
		      ToCom1(5); ToCom1(4);
		      ToCom1(0); ToCom1(0); ToCom1(1); ToCom1(0); ToCom1(0);
		      break;
	   case  6  :
		      while (!IsCom1());
		      data=ReadCom1(); if (data!=7) goto ret_label;
		      while (!IsCom1());
		      data=ReadCom1(); if (data!=8) goto ret_label;
		      ToCom1(11); ToCom1(10); ToCom1(9);
		      ToCom1(0); ToCom1(0); ToCom1(0); ToCom1(1); ToCom1(0); ToCom1(0); ToCom1(0);
		      break;
	   default  : goto ret_label;
	   }
       }
    }

ret_label:
 RestoreCom1();
}

