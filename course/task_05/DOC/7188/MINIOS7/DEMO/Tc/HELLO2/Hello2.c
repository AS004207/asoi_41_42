/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: Hello2.prj
   	    Hello2.c
	    i7188s.lib
4. Explain: (1). To detect the current operation system.
            (2). If you used this program in minios7,it will show 7188's version 
                 , "Hello I-7188!" and "1+2=3".
            (3). If you used this program in pure dos or dos-box in win98, 
                 it will show "Hello PC!, this program is not run under I-7188."
5. Hordware: 7188
It's compiled by Tony
-----------------------------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<string.h>
#include"..\LIB\i7188.h"
/*--------------------------------------------------------------------------------*/

int Add(int a,int b)
{
   return a+b;
}

void main(int argc,char *argv[])
{ int type;		/*detect the current operation system*/

  if((!(Is7188()))){		/*if it's used in dos*/
     Print("Hello PC!, this program is not run under 7188.");
     return;
  }

  type=Is7188();		/*if it's used in minios7*/
  Print("Hello 7188! (Flash memory is %d K)\n\r",type);
  Print("1+2=%d\n\r",Add(1,2));

}