/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: test7067.prj
   	    test7067.c
	    i7188s.lib
4. Explain: (1) the led of 7067 will take turn to turn on then turn off .
            (2) press 'q' or 'Q' to quit this program.
            Their situation is as fallow:

[PC]<---------->[8831]<------------------->[7520]<--------->[7067]
	RS232  (COM4)    (COM1)  RS232/RS485  (RS232)   (RS485)  (RS485)

5. Hordware: 7188 with 7067
It's compiled by Tony
-------------------------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<string.h>
#include "..\lib\i7188.h"
/*-----------------------------------------------------------------------------*/

#define TimeOut 10000L

main()
{
   int i,y;
   int type;
   int ver;
   unsigned char cmd[80], result[80];
   type=Is7188();		/*detect the current operation system*/
   if(type)
     {
     ver=GetLibVersion();
     Print("Hello 7188x! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
     }
   else
     {
     Print("Hello PC!, this program is not run under I-7188.");
     return;
     }
  InstallCom2(115200L,8,0,1);
  Init5DigitLed();
  Show5DigitLed(1,7);
  Show5DigitLed(2,0);
  Show5DigitLed(3,6);
  Show5DigitLed(4,7);
  Show5DigitLed(5,13);
  i=y=0;
  for (;;)
    {
    if (y>=7) y=0;
    DelayMs(500);
    if (y==0) strcpy(cmd,"#020001");
     else if (y==1) strcpy(cmd,"#020002");
     else if (y==2) strcpy(cmd,"#020004");
     else if (y==3) strcpy(cmd,"#020008");
     else if (y==4) strcpy(cmd,"#020010");
     else if (y==5) strcpy(cmd,"#020020");
     else if (y==6) strcpy(cmd,"#020040");

    SendCmdTo7000(2,cmd,0);
    
    y++,i++;
    if (Kbhit())
      {
       Getch();
       return;
      }
    }
}


