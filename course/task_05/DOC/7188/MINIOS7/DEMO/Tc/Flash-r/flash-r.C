/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: Flash-r.prj
   	    Flash-r.c
	    i7188s.lib
4. Explain: To read the value writing in flash memory address from 1 to 65535 .
            *To press 'q' to quit this program.
5. Hordware: 7188
It's compiled by Tony
---------------------------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<string.h>
#include"..\lib\i7188.h"
/*-------------------------------------------------------------------------------*/

void main(void)
{
 int data,type,ver;
  unsigned e=0x9000,i=0;
  char c;
  type=Is7188();	/*detect the current operation system*/
   if(type)		/*if it's used in minios7*/
     {
     ver=GetLibVersion();
     Print("Hello 7188x! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
     }
   else		/*if it's used in dos*/
     {
     Print("Hello PC!, this program is not run under I-7188.");
     return;
     } 
 
 while(i<65535){
    FlashRead(e,i);
    Print("\r\nThe value which segment %u of Flash Member is %d",i,FlashRead(e,i));
    i++;
    if (i%100==0){
    Print("\r\nPress q to quit or any key to continue...");c=Getch();
     if ((c=='q') || (c=='Q')) return;
    }
  }
}
