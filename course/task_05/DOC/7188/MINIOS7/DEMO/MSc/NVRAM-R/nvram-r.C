/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: Nvram-r.prj
   	    Nvram-r.c
	    i7188s.lib
4. Explain: To read the value writed in peer address of navram.
5. Hordware: 7188 with RTC
It's compiled by Tony
-------------------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<string.h>
#include"..\LIB\i7188.h"
/*-----------------------------------------------------------------------*/

void main(void)
{
 int addr=0;
 int type;
   int ver;
   unsigned char cmd[80], result[80];
   type=Is7188();		/*detect the current operation system*/
   if(type)			/*if it's used in minios7*/
     {
     ver=GetLibVersion();
     Print("Hello 7188x! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
     }
   else		/*if it's used in dos*/
     {
     Print("Hello PC!, this program is not run under I-7188.");
     return;
     }
 while(addr<31)
    {
     DelayMs(10);
     Print("\n\rThe valume of writing to NVRAM address %d is %d",addr,ReadNVRAM(addr));
     addr=addr++;
     }
}
