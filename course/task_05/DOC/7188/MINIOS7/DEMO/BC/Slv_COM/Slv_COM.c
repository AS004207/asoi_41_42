/* 
Demo: Slave COM (Slv_COM.c)

Compiler: BC++ 3.1
Compile mode: large
Project:
	slvcom.c
	..\lib\7188EL.lib	; you must change this lib to 
	                    ; suit the hardware that you want to use.

PC sends commands to I-8000/I-7188, and get back responses is 
a very typical application.
In this kind appliction, one COM port of I-8000/I-7188 must 
acts as a command decoder.
This demo shows how to do this and how to use another COM port or LED 
to show information to help you debug your program.

Hardware: I-8000/I-7188

[Jun,18,2002] by Kevin
*/

#include <string.h>
#include <stdio.h>
#include "..\lib\I7188.h"	/* you must change this lib to
							   suit the hardware that you want to use. */

int Receive_Str(int iPort,unsigned char* cInBuf,long lTimeout);
// Uses COM port to receive string.
// If time between two byte grater than lTimeout, abort receiving.

void ShowLED(int iNumber);
// Show number to 5-digit LED.
// Sometimes, you need some tools to help you to debug.
// The 5-digit LED is a simple tool that you can show some
// information on it to trace the program.

main(viod)
{
	int iQuit=0;
	int iLen;
	int iCommandCOMPort,iDebugCOMPort;
	unsigned char cData[100];

	iCommandCOMPort=1;
	iDebugCOMPort=2;

	InstallCom(iCommandCOMPort,115200,8,0,1);
	InstallCom(iDebugCOMPort,115200,8,0,1);
	
	Init5DigitLed();
	ShowLED(0);		// trace point 0 
	ToComStr(iDebugCOMPort,"Debug information: Start program\n\r");

	while(!iQuit)
	{
		if(IsCom(iCommandCOMPort))	// There are some data in buffer
		{
			//iLen=Receive_Str(iCommandCOMPort,3000);
			iLen=Receive_Str(iCommandCOMPort,cData,3000);
			if(iLen>0)
			{
				if(!strcmp(cData,"c1"))
				{
					ShowLED(1);	// trace point 1
					ToComStr(iDebugCOMPort,"Debug information: Command 1\n\r");
					ToComStr(iCommandCOMPort,"Command 1\n\r");
				}
				else if(!strcmp(cData,"c2"))
				{
					ShowLED(2);	// trace point 2
					ToComStr(iDebugCOMPort,"Debug information: Command 2\n\r");
					ToComStr(iCommandCOMPort,"Command 2\n\r");
				}
				else if(!strcmp(cData,"q"))
				{	
					ShowLED(3);	// trace point 3
					ToComStr(iDebugCOMPort,"Debug information: Quit program\n\r");
					Delay(100);
					iQuit=1;
				}
				else
				{
					ShowLED(4);	// trace point 4
					printCom(iDebugCOMPort,"Debug information: Unknown command : %s\n\r",cData);
					printCom(iCommandCOMPort,"Unknown command : %s\n\r",cData);
				}
			}
			else
			{
				ToComStr(iDebugCOMPort,"Debug information: Keyin timeout!!!\n\r");
				ToComStr(iCommandCOMPort,"Keyin timeout!!!\n\r");
			}
		}
	}
	RestoreCom(iCommandCOMPort);
	RestoreCom(iDebugCOMPort);
	return 0;
}
	
int Receive_Str(int iPort,unsigned char* cInBuf,long lTimeout)
{
	//receives one string that with one terminal character 0x0D.
	unsigned char cChar;
	int iIndex=0;
	unsigned long lStartTime;

	lStartTime=*TimeTicks;
	for(;;)
	{
		if(IsCom(iPort))	//check COM port
		{
			cChar=ReadCom(iPort);	//read data from COM port
			if(cChar=='\r')			//the terminal char is 0x0D
			{
				cInBuf[iIndex]=0;
				return iIndex;		//return data length
			}
			else
				cInBuf[iIndex++]=cChar;
				
			lStartTime=*TimeTicks;	
		}
		if((*TimeTicks-lStartTime)>=lTimeout)
			return -1;	//receive data timeout
	}
}


void ShowLED(int iNumber)
{
	int i;
	char cLED[5];
	
	sprintf(cLED,"%05d",iNumber);
	
	for(i=0;i<5;i++)
	{
		if (cLED[i]>='0' && cLED[i]<='9')
			cLED[i]=cLED[i]-'0';
		if (cLED[i]>='a' && cLED[i]<='f')
			cLED[i]=cLED[i]-'a'+10;
		if (cLED[i]>='A' && cLED[i]<='F')
			cLED[i]=cLED[i]-'A'+10;
		if (cLED[i]==' ')
			cLED[i]=16;
		if (cLED[i]=='-')
			cLED[i]=17;
		if (cLED[i]=='.')
			cLED[i]=18;
		Show5DigitLed(i+1,cLED[i]);
	}
}