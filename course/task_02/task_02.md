1. Документировать проект "Hello world!" используя Doxygen. Получить документацию в формате ".chm". Поместить результаты в соответствующие каталоги:

```
...
trunk
    |
    ...
    as00xxyy
          |
          |--task_02
                   |--src
                   |--doc
...
```
