///\author �������� ���������, ��-41
#include"stdafx.h"
#include<iostream>
#include"GeneralModel.h"
#include"LinearModel.h"
#include"NonlinearModel.h"
#include"PIDRegulator.h"
#include<iostream>

///\mainpage Model
///\image html LinModel.png
///\image html NonLinModel.png
/// �� �������� ����� ��� �� ���������� ������.
///���������� ������ ������������ ������� ��� ��������.

using namespace std;


void main()
{
	const double w = 40.0;

	LinearModel *linMod = new LinearModel();
	NonlinearModel *nonlinMod = new NonlinearModel(0);
	PIDRegulator *regul = new PIDRegulator(0.01, 0.5, 0.21, 0.5);

	double y = linMod->Model(0, 0);
	double u = regul->Deviation(y, w);

	for (int i = 0; i < 200; i++)
	{
		y = linMod->Model(y, u);
		u = regul->Deviation(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}
	cout << "NonLinear Model: " << endl;

	y = nonlinMod->Model(0, 0);
	u = regul->Deviation(y, w);

	for (int i = 0; i < 200; i++)
	{
		y = nonlinMod->Model(y, u);
		u = regul->Deviation(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}

	system("PAUSE");
}