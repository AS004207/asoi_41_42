#pragma once
#include "Abstact.h"
using namespace::std;

///@class Liner
///@brief Class Liner derived from Abstract 
class Liner :public Abstact
{
private:
	double array_y[11];
public:
	Liner() :array_y() {}

	///@brief Liner method solution 
	void ShowResult();
};