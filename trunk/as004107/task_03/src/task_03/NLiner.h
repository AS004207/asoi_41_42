#pragma once
#include "Abstact.h"
using namespace::std;
///@class NLiner
///@brief Class NLiner derived from Abstract 
class NLiner : public Abstact
{
private:
	double array_y[11] = {};
public:
	NLiner() :array_y() {}

	///@brief NLiner method solution 
	void ShowResult();
};
