///@file Abstract.cpp сcontains an abstract class implementation

#include "stdafx.h"
#include "Abstract.h"

extern double T0, Td, T, K, q0, q1, q2, e1, e2, e3, u, w, y;

Abstract::Abstract() {}

Abstract::~Abstract() {}

void Abstract::pid() {
	q0 = K*(1 + Td / T0);
	q1 = -K*(1 + 2 * (Td / T0) - T0 / T);
	q2 = K*(Td / T0);
	e3 = e2;
	e2 = e1;
	e1 = w - y;
	u += q0*e1 + q1*e2 + q2*e3;
}