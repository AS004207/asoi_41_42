///@file Abstract.h contains an abstract class description

#pragma once
#ifndef ABSTRACT_H
#define ABSTRACT_H

extern double T0, Td, T, K, q0, q1, q2, e1, e2, e3, u, w, y;

/*@class Abstract
Abstract class
*/

class Abstract{
public:
	///@brief PID-controller
	void pid();
	///@brief Default constructor
	Abstract();
	///@brief Destructor
	~Abstract();
protected:
	///@brief Abstract output method
	virtual void output() = 0;
};

#endif