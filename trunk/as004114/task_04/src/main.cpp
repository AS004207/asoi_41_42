///@file main.cpp contains main function of program
///@author Krivol Vladislav
#include "Linear.h"
#include "Nonlinear.h"
#include <iostream>
#include <fstream>

using namespace std;

///@param parameters for PID-controller
double T0 = 0.4, Td = 0.01, T = 0.85, K = 0.5, q0, q1, q2, e1 = 0, e2 = 0, e3 = 0, u = 0, w = 20, y = 0;

/**@mainpage PID-controller
\image html plots.png
*/

int main() {
	Linear a;
	Nonlinear b;
	a.output();
	b.output();
	return 0;
};