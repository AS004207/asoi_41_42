///@author Krivol Vladislav AS-41
///@date 19.12.2016
/**
@mainpage task_05
*\brief Program shows the string "HELLO WORLD" on the seven-segment indicator.

@file HELLO_WORLD.C
*\brief Source code realizing the output on the seven-segment indicator.
*/

#include "I7188.h"
void main()
{
while(1)
{
Show5DigitLedSeg(1,55);
Show5DigitLedSeg(2,79);
Show5DigitLedSeg(3,14);
Show5DigitLedSeg(4,14);
Show5DigitLedSeg(5,126);
DelayMs(1000);
Show5DigitLedSeg(1,79);
Show5DigitLedSeg(2,14);
Show5DigitLedSeg(3,14);
Show5DigitLedSeg(4,126);
Show5DigitLedSeg(5,0); 
DelayMs(1000);
Show5DigitLedSeg(1,14);
Show5DigitLedSeg(2,14);
Show5DigitLedSeg(3,126);
Show5DigitLedSeg(4,0); 
Show5DigitLedSeg(5,62)
DelayMs(1000);
Show5DigitLedSeg(1,14);
Show5DigitLedSeg(2,126);
Show5DigitLedSeg(3,0); 
Show5DigitLedSeg(4,62);
Show5DigitLedSeg(5,126);
DelayMs(1000);
Show5DigitLedSeg(1,126);
Show5DigitLedSeg(2,0);  
Show5DigitLedSeg(3,62); 
Show5DigitLedSeg(4,126);
Show5DigitLedSeg(5,70); 
DelayMs(1000);
Show5DigitLedSeg(1,0); 
Show5DigitLedSeg(2,62);
Show5DigitLedSeg(3,126);
Show5DigitLedSeg(4,70);
Show5DigitLedSeg(5,14);
DelayMs(1000);
Show5DigitLedSeg(1,62); 
Show5DigitLedSeg(2,126);
Show5DigitLedSeg(3,70); 
Show5DigitLedSeg(4,14); 
Show5DigitLedSeg(5,61); 
DelayMs(1000);
Show5DigitLedSeg(1,126);
Show5DigitLedSeg(2,70);
Show5DigitLedSeg(3,14);
Show5DigitLedSeg(4,61);
Show5DigitLedSeg(5,0);
DelayMs(1000);
}
}