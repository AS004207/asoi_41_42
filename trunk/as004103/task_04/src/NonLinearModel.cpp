//
// Created by vlad on 08/12/16.
//

#include "NonLinearModel.h"
#include <math.h>

void NonLinearModel::getObjectModeling() {
    for (int i = 2; i < size; i++) {
        pidController(i);
        temp[i] = (double) (0.9 * temp[i - 1] - 0.001 * temp[i - 2] * temp[i - 2] + u + sin(u));
    }
}