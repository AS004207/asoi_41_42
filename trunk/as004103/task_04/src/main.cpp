#include <iostream>
#include "LinearModel.h"
#include "NonLinearModel.h"


/**
* \mainpage
* \image html LinearGraph.png
* \image html NonLinearGraph.png
*/


using namespace std;
/**
* you can built this project using CMakeLists.txt
*/
int main() {
    LinearModel linearModel;
    linearModel.getObjectModeling();
    cout << "Line Model" << endl;
    for (int i = 1; i<20; i++) {
        cout << i << ": " << linearModel.temp[i] << endl;
    }
    cout << endl;
    NonLinearModel nonLinearModel;
    cout << "Non Line Model" << endl;
    nonLinearModel.getObjectModeling();
    for (int i = 1; i<20; i++) {
        cout << i << ": " << nonLinearModel.temp[i] << endl;
    }
    return 0;
}
