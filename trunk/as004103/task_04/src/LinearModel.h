//
// Created by vlad on 08/12/16.
//

#ifndef LAB4MMPIU_LINEARMODEL_H
#define LAB4MMPIU_LINEARMODEL_H

#include "ObjectModel.h"

class LinearModel : public ObjectModel{

public:
    void getObjectModeling();
};


#endif //LAB4MMPIU_LINEARMODEL_H
