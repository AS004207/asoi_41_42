//
// Created by vlad on 02/12/16.
//

#ifndef UNTITLED1_LINEARTEMPERATUREFUNCTION_H
#define UNTITLED1_LINEARTEMPERATUREFUNCTION_H

#include "TemperatureFunction.h"

class LinearTemperatureFunction : public TemperatureFunction {
public:
    LinearTemperatureFunction(long double heat, long double outletTemperature);
    long double getFunction();
};


#endif //UNTITLED1_LINEARTEMPERATUREFUNCTION_H
