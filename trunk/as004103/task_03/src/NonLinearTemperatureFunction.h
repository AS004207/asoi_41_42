//
// Created by vlad on 02/12/16.
//

#ifndef UNTITLED1_NONLINEARTEMPERATUREFUNCTION_H
#define UNTITLED1_NONLINEARTEMPERATUREFUNCTION_H

#include "TemperatureFunction.h"

class NonLinearTemperatureFunction : public TemperatureFunction{
public:
    NonLinearTemperatureFunction(long double heat, long double outletTemperature);
    long double getFunction();
private:
    long double previousOutletTemperature = 0.0;
};


#endif //UNTITLED1_NONLINEARTEMPERATUREFUNCTION_H
