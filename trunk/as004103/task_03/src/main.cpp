#include <iostream>
#include "LinearTemperatureFunction.h"
#include "NonLinearTemperatureFunction.h"
#include "Calculator.h"

/**
* \mainpage
* \image html LinearFunctionGraph.png
* \image html NonLinearGraph.png
*/
//you can use CMakeList.txt to build this project
int main() { //the main function
    //create function objects
    TemperatureFunction *linearTemperatureFunction = new LinearTemperatureFunction(25.0, 40);
    TemperatureFunction *nonLinearTemperatureFunction = new NonLinearTemperatureFunction(25.0, 40);
    //create calculator object
    Calculator *calculator = new Calculator();
    //calculate linear temperature function
    calculator->calculate(linearTemperatureFunction);
    std::cout << "Non linear function" << std::endl;
    //calculate non linear temperature function
    calculator->calculate(nonLinearTemperatureFunction);
    return 0;
}
