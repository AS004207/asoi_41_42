//standart library to input and output
#include <iostream>
/**
you can use CMakeList.txt to build this project
*/
int main() { //the main function
	/**
	display Hello, World!
	*/
    std::cout << "Hello, World!" << std::endl;
    /**
	return code
	*/
    return 0;
}
