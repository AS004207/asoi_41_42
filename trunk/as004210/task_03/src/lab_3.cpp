///@file
///@author Kristina Kislaya
///@15.10.2016

#include "stdafx.h"
#include "first.h"
#include "line.h"
#include "notline.h"


///@mainpage Result
///@image html C:\Users\User\Desktop\Project\25.png
///@brief creating the model
int main()
{
	double u;
	cout << "Enter start temperature u(t): ";
	cin >> u;
	line q;
	notline w;
	q.setY0(0);
	q.setY_U(u);
	q.show();
	cout << endl;
	w.setY1(15.988);
	w.setY_U(u);
	w.show();
	system("pause");
	return 0;
}
