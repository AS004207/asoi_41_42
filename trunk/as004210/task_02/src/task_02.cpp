///@file
///@author Kislaya Kristina
///@02.11.2016
///@brief include libraries
#include <iostream>
#include <conio.h>
using std::cout;
using std::endl;
///@brief Main function
int main()
{
	///@brief output on console "Hello world!"
	cout << "Hello World!" << endl;
	_getch();
	return 0;
}