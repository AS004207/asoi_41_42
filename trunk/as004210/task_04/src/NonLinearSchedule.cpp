/*! \file NonLinearSchedule.cpp
* \brief Source code including definition of the function Temp() for the class NonLinearSchedule, and also constructor and destructor for the class NonLinearShedule.
*/

#include "NonLinearSchedule.h"
#include "AbstractClass.h"
///@briefFunction modeling PID-regulator for non-linear model.
void NonLinearSchedule::Temp()
{
	double Temperature_2 = 0, Temperature_1 = 7;
	std::cout << "\nSolution of the nonlinear equation\n\n";
	std::cout.width(11);
	std::cout << "Temperature" << "  |  " << "�ontrol action\n\n";
	for (int i = 1; i <= 25; i++)
	{
		PidRegular();

		Temperature_2 = 0.9*Temperature_1 - 0.001*Temperature * Temperature + u + sin(u);
		Temperature_1 = Temperature_2;
		Temperature = Temperature_1;
		std::cout.width(11);
		std::cout << Temperature_2 << "  |  " << u << std::endl;
	}
}
///@brief Constructor for the class NonLinearSchedule.
NonLinearSchedule::NonLinearSchedule() {}
///@brief Destructor for the class NonLinearSchedule.
NonLinearSchedule::~NonLinearSchedule() {}