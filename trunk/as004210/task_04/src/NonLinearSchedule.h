/*! \file NonLinearSchedule.h
* \brief Header file including prototypes of the function Temp(), definition of the class NonLinearSchedule, description of the class NonLinearSchedule, and also constructor and destructor.
*/

#ifndef _NONLINEARSCHEDULE_H
#define _NONLINEARSCHEDULE_H
#include <math.h>
#include <iostream>
#include "AbstractClass.h"
/*!Class NonLinearSchedule realizing non-linear model.
\brief Class inharited from abstract class.
*/

class NonLinearSchedule : public AbstractClass
{
public:
	double Temperature_2 = 0, Temperature_1 = 0;
	NonLinearSchedule();
	~NonLinearSchedule();
	void NonLinearSchedule::Temp();
};
#endif

