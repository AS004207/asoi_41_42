/** \file
\brief Task_04 source code

\mainpage

\image html C:\Users\User\Desktop\task_04\7.PNG
\image html C:\Users\User\Desktop\task_04\15.PNG
\image html C:\Users\User\Desktop\task_04\30.PNG
\author Kislaya Kristina
\date 11.11.2016
*/
#include "AbstractClass.h"
#include "LinearSchedule.h"
#include "NonLinearSchedule.h"		
#include <iostream>
using namespace std;
///@briefFunction realizes creating of objects of a class and operates with them.
int main()
{
	setlocale(0, "");
	LinearSchedule a;
	a.Temp();
	NonLinearSchedule b;
	b.Temp();
	system("pause");
	return 0;
}