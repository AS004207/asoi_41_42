#include "stdafx.h"
#include "temp.h"
///@brief calculate linear function
void Line::temp()
{
	int t = 0;
	y[0] = 0;
	cout << "linear function" << endl;
	cout << "Output temperature" << "   " << "Time";
	for (t = 0; t < 11; t++) {
		y[t + 1] = 0.988*y[t] + 0.232*u;
		cout << "        \t" << y[t] << "\t" << t << endl;

	}

}
///@brief calculate nonlinear function
void nonLine::temp()
{
	int t = 0;
	y[0] = 0;
	y[1] = 0;
	cout << "nonlinear function" << endl;
	cout << "Output temperature" << "   " << "Time";
	for (t = 0; t < 11; t++) {
		y[t + 1] = 0.9*y[t] - 0.001*y[t - 1] * y[t - 1] + u + sin(u);
		cout << "        \t" << y[t] << "\t" << t << endl;

	}

}
