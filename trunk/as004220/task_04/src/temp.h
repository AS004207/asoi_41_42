#pragma once
#include "stdafx.h"
#include <iostream>
#include <cmath>
using namespace std;

///@brief abstractny class 
class AbstrCl {
public:
	double T0 = 0.1, Td = 0.02, T = 0.5, k = 0.5, q0, q1, q2, e1 = 0, e2 = 0, e3 = 0, u = 20, w = 30, y = 0;
	///@brief method for PID
	void PID()
	{
		q0 = k*(1 + (Td / T0));
		q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = k * (Td / T0);
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += (q0 * e1 + q1 * e2 + q2 * e3);

	}
	virtual void temp() = 0;
};


///@brief class nonlinear_model
///@image html nonlinear.png
///@brief calculate nonlinear function
class nonLine :public AbstrCl
{
public:

	void temp();
};

///@brief class linear_model 
///@image html linear.png
///@brief calculate linear function
class Line :public AbstrCl
{
public:

	void temp();
};

