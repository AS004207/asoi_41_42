# Для выполнения лабораторных работ используется IDE [CLion] и система сборки [CMake].

## Пример того как генерировать файлы проекта для IDE Microsoft Visual Studio 2015 CE:
```sh
$ cd target_directory
$ cmake -G "Visual Studio 14 2015 Win64"
$ dir
$ EXECUTABLE_NAME.sln
```

[CMake]: <https://cmake.org/download/>
[CLion]: <https://www.jetbrains.com/clion/>
