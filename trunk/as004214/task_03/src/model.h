/**
 \file model.h
\authors Левончук Вячеслав.
\date 20.10.2016
 \brief Файл содержит классы необходимые для реальзации модели управления.
*/

#ifndef MMPIU3_MODEL_H
#define MMPIU3_MODEL_H

/**
\brief Абстрактный класс

В данном классе присутствуют переменные вычисляемые в наших моделях управления.
А так же виртуальный метод display, который будет наследован.
*/
class ObjectModel{
protected:
    ///\brief Массив для хранения Y[t].
    long double Y[20];
    ///\brief Константное значение Ut.
    double Ut=20.5;
public:
    virtual void display()= 0;
    virtual void count()=0;
};

/**
\brief Класс для линейной модели.

Дочерний класс.
В нем реализованы методы для подсчета и вывода значений функций для линейной модели.
*/
class LinearObjectModel : public ObjectModel{
public:
    void display();
    void count();
    LinearObjectModel(){};
    ~LinearObjectModel(){};
};


/**
\brief Класс для нелинейной модели.

Дочерний класс.
В нем реализованы методы для подсчета и вывода значений функций для нелинейной модели.
*/

class NotLinearObjectModel : public ObjectModel{
public:
    void display();
    void count();
    NotLinearObjectModel(){};
    ~NotLinearObjectModel(){};
};


#endif //MMPIU3_MODEL_H
