/**
 \file main.cpp
 \brief Программа "Task 4".
\authors Левончук Вячеслав.
\date 17.11.2016
\details Выполнение задания 4. Реализация объекта управления и ПИД-регулятора.
С использованием ООП, на языке С++.
График линейной модели.
\image html lin.png
График нелинейной модели.
\image html neLin.png
*/


#include <iostream>
#include <stdlib.h>
#include "model.h"

using namespace std;


/**
Основная функция написанной программы.
В ней создаем объекты классов линейной и нелинейной модели.
А также производим вызов метода display для созданых объектов.
\return  Ноль, если успешно выполнено.
*/

int main() {
    LinearObjectModel linear;
    NotLinearObjectModel notLinear;
    linear.display();
    notLinear.display();
    system("pause");
    return 0;
}
