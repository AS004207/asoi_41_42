/**
 \file
 \brief Программа "Hello, world!".
\authors Левончук Вячеслав.
\date 20.10.2016
\details Выполнение задания 2, для получения знаний пользования Doxygen.
*/
#include <iostream>
/**
 Выполняет вывод на экран сообщения:'Hello world!'
 \return Ноль, если успешно выполнено.
 */
int main() {
    std::cout << "Hello, World!" << std::endl;
    return 0;
}