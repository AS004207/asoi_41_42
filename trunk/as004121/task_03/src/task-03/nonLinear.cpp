#include "nonLinear.h"

using namespace std;

void nonLinear::show() {
	///@brief calculating the non linear model
	cout << "Non Linear method" << endl << endl;
	for (int t = 1; t <= 10; t++) {
		Y[t + 1] = 0.9*Y[t] - 0.001*Y[t - 1] * Y[t - 1] + Ut + sin(Ut);
		cout << "Y[" << t << "]\t=\t" << Y[t] << endl;
	}
	cout << endl;
}
