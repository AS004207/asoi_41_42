#pragma once
#include <iostream>

///@brief abstract class

class temp
{
protected:
	double Y[11] = { 0 , 0 };
	double Ut = 100;
public:
	///@show virtual method
	virtual void show() = 0;

	~temp() {};
};
