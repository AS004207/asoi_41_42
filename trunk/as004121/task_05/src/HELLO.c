#include"i7188.h"

///@mainpage  Hello World
///@file HELLO.C
///@author Mokej Dima
///@date 17.12.2016
///@brief This program will display on LED string "Hello World"
void main()
{
while(1)
{
Show5DigitLedSeg(1,55); //H
Show5DigitLedSeg(2,79); //E
Show5DigitLedSeg(3,14); //L
Show5DigitLedSeg(4,14); //L
Show5DigitLedSeg(5,126); //o
DelayMs(1000);
Show5DigitLedSeg(1,79); //E
Show5DigitLedSeg(2,14); //L
Show5DigitLedSeg(3,14); //L
Show5DigitLedSeg(4,126); //o
Show5DigitLedSeg(5,0); //
DelayMs(1000);
Show5DigitLedSeg(1,14); //L
Show5DigitLedSeg(2,14); //L
Show5DigitLedSeg(3,126); //o
Show5DigitLedSeg(4,0); //
Show5DigitLedSeg(5,14); //L
DelayMs(1000);
Show5DigitLedSeg(1,14); //L
Show5DigitLedSeg(2,126); //o
Show5DigitLedSeg(3,0); //
Show5DigitLedSeg(4,14); //L
Show5DigitLedSeg(5,14); //L
DelayMs(1000);
Show5DigitLedSeg(1,126); //o
Show5DigitLedSeg(2,0); //
Show5DigitLedSeg(3,14); //L
Show5DigitLedSeg(4,14); //L
Show5DigitLedSeg(5,6); //l
DelayMs(1000);
Show5DigitLedSeg(1,0); //
Show5DigitLedSeg(2,14); //L
Show5DigitLedSeg(3,14); //L
Show5DigitLedSeg(4,6); //l
Show5DigitLedSeg(5,126); //o
DelayMs(1000);
Show5DigitLedSeg(1,14); //L
Show5DigitLedSeg(2,14); //L
Show5DigitLedSeg(3,6); //l
Show5DigitLedSeg(4,126); //o
Show5DigitLedSeg(5,70); //r
DelayMs(1000);
Show5DigitLedSeg(1,14); //L
Show5DigitLedSeg(2,6); //l
Show5DigitLedSeg(3,126); //o
Show5DigitLedSeg(4,70); //r
Show5DigitLedSeg(5,14); //L
DelayMs(1000);
Show5DigitLedSeg(1,6); //l
Show5DigitLedSeg(2,126); //o
Show5DigitLedSeg(3,70); //r
Show5DigitLedSeg(4,14); //L
Show5DigitLedSeg(5,61); //d
DelayMs(1000);
Show5DigitLedSeg(1,126); //o
Show5DigitLedSeg(2,70); //r
Show5DigitLedSeg(3,14); //L
Show5DigitLedSeg(4,61); //d
Show5DigitLedSeg(5,0); //
DelayMs(1000);
Show5DigitLedSeg(1,70); //r
Show5DigitLedSeg(2,14); //L
Show5DigitLedSeg(3,61); //d
Show5DigitLedSeg(4,0); //
Show5DigitLedSeg(5,0); //
DelayMs(1000);
Show5DigitLedSeg(1,14); //L
Show5DigitLedSeg(2,61); //d
Show5DigitLedSeg(3,0); //
Show5DigitLedSeg(4,0); //
Show5DigitLedSeg(5,0); //
DelayMs(1000);
Show5DigitLedSeg(1,61); //d
Show5DigitLedSeg(2,0); //
Show5DigitLedSeg(3,0); //
Show5DigitLedSeg(4,0); //
Show5DigitLedSeg(5,0); //
DelayMs(1000);
Show5DigitLedSeg(1,0); //
Show5DigitLedSeg(2,0); //
Show5DigitLedSeg(3,0); //
Show5DigitLedSeg(4,0); //
Show5DigitLedSeg(5,0); //
DelayMs(1000);
Show5DigitLedSeg(1,0); //
Show5DigitLedSeg(2,0); //
Show5DigitLedSeg(3,0); //
Show5DigitLedSeg(4,0); //
Show5DigitLedSeg(5,55); //H
DelayMs(1000);
Show5DigitLedSeg(1,0); //
Show5DigitLedSeg(2,0); //
Show5DigitLedSeg(3,0); //
Show5DigitLedSeg(4,55); //H
Show5DigitLedSeg(5,79); //E
DelayMs(1000);
Show5DigitLedSeg(1,0); //
Show5DigitLedSeg(2,0); //
Show5DigitLedSeg(3,55); //H
Show5DigitLedSeg(4,79); //E
Show5DigitLedSeg(5,14); //L
DelayMs(1000);
Show5DigitLedSeg(1,0); //
Show5DigitLedSeg(2,55); //H
Show5DigitLedSeg(3,79); //E
Show5DigitLedSeg(4,14); //L
Show5DigitLedSeg(5,14); //L
DelayMs(1000);
}
}