///@mainpage  A console application that displays the "Hello World!"
///@file
///@author Mokey Dima
///@date 20.11.2016
#include "iostream"
///@brief We define an entry point of the program
int main()
{
	///@brief Display message "Hello, world!"
	std::cout << "Hello, World!" << std::endl;
	///@brief We calling "pause" of console
	system("pause");
	return 0;
}
