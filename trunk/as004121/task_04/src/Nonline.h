#pragma once
#include "ControlObject.h"

///@class Nonline
///@brief Class Nonline derived from ControlObject
class NonLine : public ControlObject
{
public: 
	void modeling();

	};
///@brief Nonline method solution