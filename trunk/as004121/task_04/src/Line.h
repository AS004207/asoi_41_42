#pragma once
#include "ControlObject.h"

///@class Line
///@brief Class Line derived from ControlObject
class line : public ControlObject
{
public:

///@brief Line method solution 
	void modeling();
};
