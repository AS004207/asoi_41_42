///@file
/**
@mainpage
@author Alexandr Martynov
@date 21.10.2016

Graphics of line and nonline models
@image html line.bmp
@image html nonline.bmp
*/

#include "stdafx.h"
#include <iostream>
#include "MMYP.h"
using namespace std;
///@brief in function main applied the metods of classes line_temperature and dif_temperature
int main() {
	line_temperature t1(30, 50);
	for (int i = 0; i<10; i++) {
		cout << i + 1 << "	";
		t1.calculate();
	}
	cout << endl;
	dif_temperature t2(30, 50);
	for (int i = 0; i<10; i++) {
		cout << i + 1 << "	";
		t2.calculate();
	}
	system("pause");
	return 0;
}
