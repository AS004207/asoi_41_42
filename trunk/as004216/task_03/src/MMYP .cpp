// MMYP.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#include "MMYP.h"

temperature::temperature(double u1, double y1) {
		u = u1;
		y = y1;
	}
temperature::~temperature() {
		u = NULL;
		y = NULL;
	}
///@brief function calculate count and display the value of line function	
	void line_temperature::calculate() {
		y = 0.988*y + 0.232*u;
		cout << y << endl;
	}
///@brief function calculate count and display the value of nonline function
	void dif_temperature::calculate() {
		double tmp = y;
		y = 0.9*y - 0.001*y_old + u + sin(u);
		y_old = tmp;
		cout << y << endl;
	}
