#include "Parent.h"

///brief Function which count common parameters 
void Parent::PidController()
{
	q0 = K*(1 + Td / T0);
	q1 = -K *(1 + 2 * Td / T0 - T0 / Ti);
	q2 = K * (Td / T0);
	e3 = e2;
	e2 = e1;
	e1 = w - y;
	u += q0 * e1 + q1 * e2 + q2 * e3;
}