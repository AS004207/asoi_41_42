#include "Nonlinear.h"
#include <iostream>
#include <math.h>

using namespace::std;

///@brief Nonlinear method solution 
void Nonlinear::ShowResult() {
	for (int i = 1; i <= 10; i++)
	{
		if (i > 1)
		{
			array_y[i] = 0.9*array_y[i - 1] - 0.001*array_y[i - 2] * array_y[i - 2] + ut + sin(ut);
		}
	}

	///@brief Nonlinear output 
	cout << "Nonlinear solution: " << endl;
	for (int i = 1; i <= 10; i++)
	{
		cout << "t=" << i << " y=" << array_y[i] << endl;
	}
}