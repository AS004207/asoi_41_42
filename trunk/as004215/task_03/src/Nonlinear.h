#pragma once
#include "Parent.h"
using namespace::std;
///@class Nonlinear
///@brief Class Nonlinear derived from Parent 
class Nonlinear : public Parent
{
private:
	double array_y[11] = {};
public:
	Nonlinear() :array_y() {}

	///@brief Nonlinear method solution 
	void ShowResult();
};

