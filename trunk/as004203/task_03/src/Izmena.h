///@file
#pragma once
#include <iostream>

///@brief abstract class
class abstraktnyi
{
public:
///@show virtual method
	virtual void show() = 0;
protected:
	double y[10];
};