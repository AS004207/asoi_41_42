///@file
///@Author Valentej Andrej Nikolaevich
///@25.10.2016
#include "lineinayaModel.h"
#include "neLineinayaModel.h"
#include <iostream>

using namespace std;
///@param input temperature
double u = 200;

///@mainpage Model of Temperature 
///@image html lin.png
///@image html neLin.png
///@brief We create objects and display them on the screen
int main()
{
	

	setlocale(0, "Russian");
	lineinayaModel lin;
	neLineinayaModel nelin;
	lin.show();
	cout << endl << endl;
	nelin.show();
	system("pause");
	return 0;
};