/** \file LinearModel.cpp
*  \author Vladislav Kondratuk (as004113) 
*  \brief Implementation of the LinearModel class.
*/
#include "LinearModel.h"

///@brief Implement constructor with paramethers.
LinearModel::LinearModel(double inputValue, double outputValue,int numberIteration) :TemperatureSystem(inputValue,outputValue,numberIteration){}

///@brief Implement method calculate.
void LinearModel::calculate()
{
	for (int i = 0; i < numIteration; i++)
	{
		pid();
		systemOutputValue = 0.988*systemOutputValue + 0.232*systemInputValue;
		std::cout << systemOutputValue << std::endl;
	}

}
