/** \file LinearModel.h
*  \author Vladislav Kondratuk (as004113) 
*  \brief Declaration of the LinearModel class.
*/

#pragma once
#include "TemperatureSystem.h"

///@brief Class LinearModel inherit from abstract class TemperatureSystem.
class LinearModel :
	public TemperatureSystem
{
public:
	///@brief Declare constructor with paramethers.
	LinearModel(double, double, int);
	///@brief Declare inherited function.
	void calculate();
};

