/**\file NonlinearModel.cpp
*  \author Vladislav Kondratuk (as004113) 
*  \brief Implementation of the NonlinearModel class.
*/
#include "NonlinearModel.h"
#include<cmath>

///@brief Implement constructor with paramethers.
NonlinearModel::NonlinearModel(double inputValue, double outputValue, int numberIteration) :TemperatureSystem(inputValue, outputValue, numberIteration)
{
	prevValue = outputValue;
}

///@brief Implement method calculate.
void NonlinearModel::calculate()
{
	double temporaryValue;
	for (int i = 0; i < numIteration; i++)
	{
		pid();
		temporaryValue = systemOutputValue;
		systemOutputValue = 0.9*systemOutputValue - 0.001*prevValue*prevValue + systemInputValue + sin(systemInputValue);
		prevValue = temporaryValue;
		std::cout << systemOutputValue << std::endl;
	}
	
}