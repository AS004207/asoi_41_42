/** \file Main.cpp
*  \author Vladislav Kondratuk (as004113) 
*  \brief Consist main function.From this function program begin executing.
*/


#include<iostream>
#include"LinearModel.h"
#include"NonlinearModel.h"

using namespace std;

/**
* \mainpage
* \image html C:\Users\User\Documents\asoi_41_42\trunk\as004113\task_04\src\ControlObject\ControlObject\linearModel.png
* \image html C:\Users\User\Documents\asoi_41_42\trunk\as004113\task_04\src\ControlObject\ControlObject\nonlinearModel.png
* \image html C:\Users\User\Documents\asoi_41_42\trunk\as004113\task_04\src\ControlObject\ControlObject\program.png
*/

int main()
{
	///@brief Create linear and nonlinear objects.
	LinearModel linear = LinearModel(20, 0,6);
	NonlinearModel nonlinear = NonlinearModel(20, 0,6);

	cout << "Linear model" << endl;
	linear.calculate();
	cout << endl << "NonLinear model" << endl;
	nonlinear.calculate();

	system("pause");
	return 0;
}