/**\file NonlinearModel.cpp
*  \author Vladislav Kondratuk  (as004113)
*  \brief Implementation of the NonlinearModel class.
*/
#include "NonlinearModel.h"
#include<cmath>

///@brief Implement constructor with paramethers.
NonlinearModel::NonlinearModel(double inputValue, double outputValue) :TemperatureSystem(inputValue, outputValue)
{
	prevValue = outputValue;
}

///@brief Implement method calculate.
double NonlinearModel::calculate()
{
	double temporaryValue = systemOutputValue;
	systemOutputValue = 0.9*systemOutputValue - 0.001*prevValue + systemInputValue + sin(systemInputValue);
	prevValue = temporaryValue;
	return systemOutputValue;
}