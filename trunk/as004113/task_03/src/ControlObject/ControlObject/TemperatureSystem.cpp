/**\file TemperatureSystem.cpp
*  \author Vladislav Kondratuk  (as004113)
*  \brief Implementation of the NTemperatureSystem class.
*/
#include "TemperatureSystem.h"

///@brief Implement abstract class constructor.
TemperatureSystem::TemperatureSystem(double inputValue, double outputValue)
{
	systemInputValue = inputValue;
	systemOutputValue = outputValue;
}