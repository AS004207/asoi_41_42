///@file
///@author Uglick Dmitry
///@27.10.2016
#include <iostream>
#include <math.h>

using namespace std;
/**@class AbstactClass 
����������� ����� AbstactClass.*/
class AbstactClass {
public:
	float Temperature[20]; float HeatQuantity = 5; float a, b, c, d, k, n;
protected:
 /** ����������� �����, ��������� ���������� � �������� �����������.*/
	virtual void temperature() = 0;
}; 
/** ��������� ����� �� ��������(������������) ��� ���������� �������� ������.*/
class LinearModel : public AbstactClass
{
public:
      /** �����, � ������� �������� ���������� ���������� �����������. */
	void temperature()
	{
		Temperature[0] = 0;
		cout << "Linear Model\n\n";
		cout.width(11);
		cout << "Temperature" << "  |  " << "T\n\n";
		for (int i = 0; i <= 20; i++)
		{
			k =Temperature[i]; n = HeatQuantity;
			Temperature[i + 1] = 0.988*k + 0.232*n;
			cout.width(11);
			cout << Temperature[i] << "  |  " << i << endl;
		}
	}
/**���������� ������ */
	~LinearModel() 
	{}
};
/** @class NotLinearModel
��������� ����� �� ��������(������������) ��� ���������� ���������� ������. */
class NotLinearModel : public AbstactClass
{
public:
     /** �����, � ������� �������� ���������� ���������� �����������.*/
	void temperature() 
	{
		Temperature[0] = 0;
		Temperature[1] = 0;
		a, b, c, d;
		cout << "\nNotLinear Model\n\n";
		cout.width(11);
		cout << "Temperature" << "  |  " << "T\n\n";
		for (int i = 1; i <= 20; i++)
		{ 
			a = Temperature[i]; b = 0.001*c; c = Temperature[i - 1]; d = HeatQuantity;
			Temperature[i + 1] = 0.9*a - b * c + d+sin(d);
			cout.width(11);
			cout << Temperature[i] << "  |  " << i << endl;
		}
	}
/** ���������� ������ */
	~NotLinearModel() 
	{}
};
/**@mainpage ������
@image html NotLinearModel.png
\details ����������� �������������� �� ������� �������� ���������� �.�. ������������ ��������, ������� ���������� �����������
@image html LinearModel.png
\details ����������� �������������� �� ������� �������� �������� �.�. ��� ��������� ������� ���������� �����������
*/
/** �������, � ������� ����������: 1)�������� �������� ������ � 2)���������� ������� ��� ����.*/
int main() 
{ 
	LinearModel One;
	NotLinearModel Two;
	One.temperature();
	Two.temperature();
	system("pause");
	return 0;
};