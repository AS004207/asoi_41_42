#define    IN_BUF_SIZE      1024

typedef unsigned int  uint;
typedef unsigned int  WORD;
typedef unsigned char uchar;
typedef unsigned char BYTE;
typedef unsigned long ulong;
typedef unsigned long DWORD;

#define    NoError	    0
#define    InitPinIsOpen    0
#define    InitPinIsNotopen 1
#define    QueueIsEmpty     0
#define    QueueIsNotEmpty  1
#define    PortError	   -1
#define    DataError	   -2
#define    ParityError	   -3
#define    StopError	   -4
#define    TimeOut	   -5
#define    QueueEmpty	   -6
#define    QueueOverflow   -7
#define    PosError	   -8
#define    AddrError	   -9
#define    BlockError	   -10
#define    WriteError	   -11
#define    SegmentError    -12
#define    BaudRateError   -13
#define    CheckSumError   -14
#define    ChannelError    -15
#define    BaudrateError   -16
#define    TriggerLevelError   -17
#define    DateError    -18
#define    TimeError    -19
#define    TimeIsUp        1

#ifndef __FILE_DATA__
#define __FILE_DATA__
typedef struct  {
  unsigned mark;   /* 0x7188 -> is file */
  unsigned char fname[12];
  unsigned char year;
  unsigned char month;
  unsigned char day;
  unsigned char hour;
  unsigned char minute;
  unsigned char sec;
  unsigned long size;
  char far *addr;
  unsigned CRC;
  unsigned CRC32;
} FILE_DATA;
#endif


#define SEND_CMD(port,cmd,timeout,checksum)  SendCmdTo7000(port,cmd,checksum)
#define RECEIVE_CMD ReceiveResponseFrom7000

extern char hex_to_ascii[16];

#ifdef __cplusplus
extern "C" {
#endif

void InitLib(void);
int  InstallCom(int port, unsigned long baud, int data, int parity, int stop);
int  InstallCom3(unsigned long baud, int data, int parity);
int  InstallCom4(unsigned long baud, int data, int parity);
int  RestoreCom(int port);
int  RestoreCom3(void);
int  RestoreCom4(void);
int  IsCom(int port);
int  IsCom3(void);
int  IsCom4(void);
int  ReadCom(int port);
int  ReadCom3(void);
int  ReadCom4(void);
int  ToCom(int port, int data);
int  ToCom3(int data);
int  ToCom4(int data);
int ToComBufn(int port,char *buf,int no);
int ToCom3Bufn(char *buf,int no);
int ToCom4Bufn(char *buf,int no);
int  ToComStr(int port, char *str);
int  ToCom3Str(char *str);
int  ToCom4Str(char *str);
int  ClearCom(int port);
int  ClearCom3(void);
int  ClearCom4(void);
int  WaitTransmitOver(int port);
int  WaitTransmitOver3(void);
int  WaitTransmitOver4(void);

int  ReadInitPin(void);
void LedOff(void);
void LedOn(void);
void Init5DigitLed(void);
int  Show5DigitLed(int position, int value);
int  Show5DigitLedSeg(int pos, unsigned char data);
void DelayTimeMs(unsigned int time);

int  ReadNVRAM(int addr);
int  WriteNVRAM(int addr, int data);

int  WriteEEP(int block, int addr, int data);
int  ReadEEP(int block, int addr);
void EnableEEP(void);
void ProtectEEP(void);

void EnableWDT(void);
void RefreshWDT(void);
void DisableWDT(void);

int FlashReadId(void);
int FlashWrite(unsigned int seg, unsigned int offset, char data);
int FlashErase(unsigned int seg);
int FlashRead(unsigned int seg, unsigned int offset);

void Delay(unsigned ms);
void Delay_1(unsigned ms);

int Is7188(void);
int Getch(void);
int Kbhit(void);
int Ungetch(int key);
void Putch(int data);
void Puts(char *str);
int Print(char *fmt,...);

int ascii_to_hex(char ascii);
int SendCmdTo7000(int iPort, unsigned char *cCmd, int iChksum);
int ReceiveResponseFrom7000(int iPort, unsigned char *cCmd, long lTimeout, int iChksum);

void far *_MK_FP(unsigned segment,unsigned offset);
int IsResetByWatchDogTimer(void);
int IsResetByPowerOff(void); /* for bios date 12/12/98 or later */

int Show5DigitLedWithDot(int pos, int data);
void Set5DigitLedTestMode(int mode);
void Set5DigitLedIntensity(int mode);
void Disable5DigitLed(void);
void Enable5DigitLed(void);
unsigned GetLibVersion(void);

/* 01/07/1999  Add Timer function */
int TimerOpen(void);
int TimerClose(void);
void TimerResetValue(void);
unsigned long TimerReadValue(void);
void DelayMs(unsigned t);
int StopWatchReset(int channel);
int StopWatchStart(int channel);
int StopWatchStop(int channel);
int StopWatchPause(int channel);
int StopWatchContinue(int channel);
int StopWatchReadValue(int channel,unsigned long *value);
int CountDownTimerStart(int channel,unsigned long count);
int CountDownTimerReadValue(int channel,unsigned long *value);
void InstallUserTimer(void (*fun)(void));
void InstallUserTimer1C(void (*fun)(void));

int DataSizeInCom(int port);
int DataSizeInCom3(void);
int DataSizeInCom4(void);
/* New for Ver 1.13 */
/* 07-23-1999 add 4 function for COM1 /DTR,/RTS output pins */
void SetRtsActive(void);
void SetRtsInactive(void);

/* [New for Ver 1.14] */
/**** 07/27/1999 add 5 function for COM1/2/3/4 formated output ****/
int printCom(int port,char *fmt,...);
int printCom3(char *fmt,...);
int printCom4(char *fmt,...);
/* 08/06/1999 add 2 function for COM1 CTS,DSR input pins */
int GetCtsStatus(void);

/* 08/25/1999 add 5 function for COM1-4 */
int IsTxBufEmpty(int port);
int IsTxBufEmpty3(void);
int IsTxBufEmpty4(void);
int IsCom3OutBufEmpty(void);
int IsCom4OutBufEmpty(void);

/* 10/19/1999 add 5 function for COM1/2/3/4 change baudrate */
int SetBaudrate(int port,unsigned long baud);
int SetBaudrate3(unsigned long baud);
int SetBaudrate4(unsigned long baud);

/* 10/20/1999 */
/* (1) add 10 function for COM1/2/3/4 send/detect BREAK signal */
int SendBreak(int port,unsigned timems);
void SendBreak3(unsigned timems);
void SendBreak4(unsigned timems);
int IsDetectBreak(int port);
int IsDetectBreak3(void);
int IsDetectBreak4(void);

/* (2) Add 3 functions for clear COM1/2/3/4 output buffer */
int ClearTxBuffer(int port);
void ClearTxBuffer3(void);
void ClearTxBuffer4(void);


/* 12/29/1999 */
/*
  Add functions for ReadOnly file system of 7188R/7188X
*/
int GetFileNo(void);
int GetFileName(int no,char *fname);
char far *GetFilePositionByNo(int no);
char far *GetFilePositionByName(char *fname);
FILE_DATA far *GetFileInfoByNo(int no);           /* 04/11/2000 */
FILE_DATA far *GetFileInfoByName(char *fname);    /* 04/11/2000 */

/*
 02/15/2000
 Add functions for system DATE/TIME
*/
void GetTime(int *hour,int *minute,int *sec);
int SetTime(int hour,int minute,int sec);
void GetDate(int *year,int *month,int *day);
int SetDate(int year,int month,int day);

void ClockHighLow(void);
void ClockHigh(void);
void ClockLow(void);

/*
 03/15/2000
 Add function Scanf ...
*/
int LineInput(char *buf,int maxlen); /* input one line from StdInput */
void ResetScanBuffer(void); /* Set Scanf to use default buffer(maxlen=80) */
void SetScanBuffer(unsigned char *buf,int len); /* Set user's buffer for Scanf*/
int Scanf(char *fmt, ...); /* like C's scanf */

/*
 06/09/2000
 Add function for COM3/COM4
  COM3 for 7522/7523 only
  COM4 for 7523 only
*/
int Set485DirToTransmit(int port);
int Set485DirToReceive(int port);
void SetCom2AutoDir(void);
void ResetCom2AutoDir(void);
void SetCom1AutoDir(void);
void ResetCom1AutoDir(void);

int InstallCom1(unsigned long baud, int data, int parity, int stop);
int RestoreCom1(void);
int IsCom1(void);
int ToCom1(int data);
int ToCom1Str(char *str);
int ToCom1Bufn(char *buf,int no);
int printCom1(char *fmt,...);
void ClearTxBuffer1(void);
int SetCom1FifoTriggerLevel(int level);
int SetBaudrate1(unsigned long baud);
int ReadCom1(void);
int ClearCom1(void);
int DataSizeInCom1(void);
int WaitTransmitOver1(void);
int IsTxBufEmpty1(void);
int IsCom1OutBufEmpty(void);
void SetDtrActive1(void);
void SetDtrInactive1(void);
void SetRtsActive1(void);
void SetRtsInactive1(void);
int GetCtsStatus1(void);
int GetDsrStatus1(void);

int InstallCom2(unsigned long baud, int data, int parity, int stop);
int RestoreCom2(void);
int IsCom2(void);
int ToCom2(int data);
int ToCom2Str(char *str);
int ToCom2Bufn(char *buf,int no);
int printCom2(char *fmt,...);
void ClearTxBuffer2(void);
int SetCom2FifoTriggerLevel(int level);
int SetBaudrate2(unsigned long baud);
int ReadCom2(void);
int ClearCom2(void);
int DataSizeInCom2(void);
int WaitTransmitOver2(void);
int IsTxBufEmpty2(void);
int IsCom2OutBufEmpty(void);

/*
  06/15/2000
 Add function for COM3
  COM3 for 7522/7523 only
*/
void SetRtsActive1(void);
void SetRtsInactive1(void);
int GetCtsStatus1(void);

/*
 10/16/2000  start Ver 1.17
 Add new EEPROM functions
*/
void EE_WriteEnable(void);
void EE_WriteProtect(void);
int EE_RandomRead(int Block,int Addr);
int EE_ReadNext(int Block);
int EE_MultiRead(int StartBlock,int StartAddr,int no,char *databuf);
int EE_RandomWrite(int Block,int Addr,int Data);
int EE_MultiWrite(int Block,int Addr,int no,char *Data);


/*
 11/20/2000  start Ver 1.18
*/
void LedToggle(void);  /* 11/20/2000 Add */
#ifndef __MEMMAP__
#define __MEMMAP__
typedef struct {
  int type;
  unsigned addr;
  unsigned word;
} MemMap;
#endif
void InstallCom3ForTouch200(void);
void RestoreCom3ForTouch200(void);
void ToTouch(int type,int addr,unsigned data);
int IsTouch(void);
MemMap *ReadTouch(void);

/*
 08/01/2001
 Add new timer functions
*/
/* for StopWatch [計時碼表] */
#ifndef _T_STOPWATCH_
#define _T_STOPWATCH_
typedef struct {
 ulong ulStart,ulPauseTime;
 uint  uMode;  /* 0: pause, 1:run(start) */
} STOPWATCH;
#endif

void T_StopWatchStart(STOPWATCH *sw);
ulong T_StopWatchGetTime(STOPWATCH *sw);
void T_StopWatchPause(STOPWATCH *sw);
void T_StopWatchContinue(STOPWATCH *sw);

/* For CountDown Timer[倒數計時] */
#ifndef _T_COUNTDOWNTIMER_
#define _T_COUNTDOWNTIMER_
typedef struct {
 ulong ulTime,ulStartTime,ulPauseTime;
 uint  uMode;  /* 0: pause, 1:run(start) */
} COUNTDOWNTIMER;
#endif

void T_CountDownTimerStart(COUNTDOWNTIMER *cdt,ulong timems);
void T_CountDownTimerPause(COUNTDOWNTIMER *cdt);
void T_CountDownTimerContinue(COUNTDOWNTIMER *cdt);
int T_CountDownTimerIsTimeUp(COUNTDOWNTIMER *cdt);
ulong T_CountDownTimerGetTimeLeft(COUNTDOWNTIMER *cdt);
extern unsigned long far *TimeTicks;


#ifdef __cplusplus
}
#endif
