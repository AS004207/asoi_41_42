#include "stdafx.h"
#include "NotLinear.h"


NotLinear::NotLinear()
{
	calculatelinearModel(35, 6);
}


NotLinear::~NotLinear()
{
}


///@brief calculate linear model
///@brief y = start temperature
///@brief w = expected temperature
///@brief q0,q1,q2 = parameters of regulator
void NotLinear::calculatelinearModel(double y, double w)
{
	q0 = calcq0(0.01, 1, 1);
	q1 = calcq1(0.01, 1, 1, 1);
	q2 = calcq2(0.01, 1, 1);
	cout << "Not Linear model: " << "K: " << 0.01 << " T: " << 1 << " Td: " << 1 << " T0: " << 1 << " y: " << 35 << " w: " << 6 << endl;

	for (int i = 0; i < 20; i++)
	{
		cout << y << endl;
		du = q0*e + q1*e1 + q2*e2;
		u = u1 + du;
		y = 0.9*y1 - 0.001*pow(y2, 2) + u + sin(u2);
		u2 = u1;
		u1 = u;
		y2 = y1;
		y1 = y;
		e2 = e1;
		e1 = e;
		e = w - y;

	}

}