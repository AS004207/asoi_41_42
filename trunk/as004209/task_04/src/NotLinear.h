#pragma once
#include "BaseClass.h"
#include <iostream>
#include <math.h>

using namespace std;
class NotLinear :
	public BaseClass
{
public:
	NotLinear();
	~NotLinear();
	void calculatelinearModel(double y, double w);

private:
	double q0 = 0, q1 = 0, q2 = 0, e = 0, e1 = 0, e2 = 0, du = 0, u = 1, u1 = 1, u2 = 0, y1 = 0, y2 = 0;
};