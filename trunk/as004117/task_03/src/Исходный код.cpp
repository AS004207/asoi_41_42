///@file
///@Author Makarava Daria
///@26.11.2016
#include "linear.h"
#include "nonlinear.h"
#include <iostream>

using namespace std;
///@param input temperature
double u = 200;

///@mainpage Model of Temperature 
///@image html C:\line.jpg
///@image html C:\NonLiner.jpg
///@brief We create objects and display them on the screen
int main()
{


	setlocale(0, "Russian");
	linear lin;
	nonlinear non_lin;
	lin.show();
	cout << endl << endl;
	non_lin.show();
	system("pause");
	return 0;
};