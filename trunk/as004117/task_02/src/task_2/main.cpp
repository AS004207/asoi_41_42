/**
@mainpage  A console application that displays the "Hello World!"
@file
@author Makarava Daria
@date 30.10.2016
*/
#include<iostream>
/**
@brief We define an entry point in the program
*/
using namespace std;
int main()
{
	/**
	@brief Display message "Hello,world"
	*/
	cout << "Hello world!\n";
	///@brief We set the display console
	system("pause");
}