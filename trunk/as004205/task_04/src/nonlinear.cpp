#include "nonlinear.h"
#include <iostream>
#include <fstream>
using namespace std;

void nonlinear::show()
{
	ofstream fout("nonlinear.txt", ios::out);
	cout << "Nonlinear:" << endl;
	cout << "y" << "\t" << "u" << "\t" << "t" << endl;
	for (int t = 0; t < 50; t++)
	{
		///@brief calculating parametrs
		PidController();
		y2 = 0.9*y1 - 0.001*y * y + u + sin(u);
		y1 = y2;
		y = y1;
		///@brief showing output
		cout << t << "\t" << y2 << "\t" << u << endl;
		///@brief writing output to file
		fout << t << "\t" << y2 << "\t" << u << endl;
	}
}