#include "linear.h"
#include <iostream>
#include <fstream>
using namespace std;

void linear::show()
{
	ofstream fout("linear.txt", ios::out);
	cout << "Linear:" << endl;
	cout << "y" << "\t" << "u" << "\t" << "t" << endl;
	for (int t = 0; t < 50; t++)
	{
		///@brief calculating parametrs
		PidController();
		y1 = 0.988*y + 0.232*u;
		y = y1;
		///@brief showing output
		cout << t << "\t" << y1 << "\t" << u << endl;
		///@brief writing output to file
		fout << t << "\t" << y1 << "\t" << u << endl;
	}
}