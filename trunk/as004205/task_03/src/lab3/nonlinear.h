#pragma once
#include "abstract.h"

///@brief Derived class
class nonlinear : public abstract
{
public:
	///@brief method for calculating nonlinear model
	void fill();
	///@brief method for displaying nonlinear model
	void show();
};