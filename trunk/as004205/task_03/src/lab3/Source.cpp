﻿/**
@file
@author Artsiom Denisiuk
@date 21.10.2016
@mainpage Temperature models
@image html linear.png
@image html nonlinear.png
*/
#include <iostream>
#include "abstract.h"
#include "linear.h"
#include "nonlinear.h"
using namespace std;

///@brief entry point of the program
int main()
{
	linear lin;
	nonlinear nonlin;
	///@brief calculating linear model
	lin.fill();
	///@brief displaying linear model
	lin.show();
	cout << endl;
	///@brief calculating nonlinear model
	nonlin.fill();
	///@brief displaying nonlinear model
	nonlin.show();
	return 0;
};