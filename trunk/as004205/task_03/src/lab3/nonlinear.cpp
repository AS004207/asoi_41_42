#include "nonlinear.h"
#include <iostream>
using namespace std;

void nonlinear::fill()
{
	y[0] = 0;
	y[1] = 0;
	for (int t = 1; t < 14; t++)
	{
		y[t + 1] = 0.9*y[t] - 0.001*y[t - 1] * y[t - 1] + u + sin(u);
	}
}

void nonlinear::show()
{
	cout << "Nonlinear:" << endl;
	cout << "y" << "\t" << "u" << "\t" << "t" << endl;
	for (int t = 0; t < 15; t++)
	{
		cout << t << "\t" << y[t] << "\t" << u << endl;
	}
}