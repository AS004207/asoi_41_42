#pragma once
#include "abstract.h"

///@brief Derived class
class linear : public abstract
{
public:
	///@brief method for calculating linear model
	void fill();
	///@brief method for displaying linear model
	void show();
};