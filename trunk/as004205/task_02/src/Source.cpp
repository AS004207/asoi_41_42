/**
@mainpage  This application displays "Hello World!"
@file
@author Artsiom Denisiuk
@date 15.10.2016
*/
#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
	
	///@brief Display message "Hello,world!"
	cout << "Hello, world!" << endl;
	///@brief pause the console screen
	system("pause");
	return 0;
}