#pragma once
#include <iostream>

///@brief abstract class

class TemperatureChange
{
protected:
	int NofY;
	double *Y;
	double *Ut;
public:
	///@show virtual method
	virtual void show() = 0;
	///@show Constructor
	TemperatureChange(int N=10,int U=10) {
		NofY = N;
		Ut = new double;
		*Ut = U;
		Y = new double[NofY];
		Y[0] = -1;
	}
	///@show Destructor
	~TemperatureChange()
	{
		delete Ut;
		if (Y != NULL)
			delete[NofY]Y;
	}
};

