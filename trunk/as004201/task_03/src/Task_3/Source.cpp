#include "LinearEquation.h"
#include "NonLinearEquation.h"
#include <iostream>

///@file
///@21.10.2016
///@mainpage Model of Temperature 
///@brief It's a created objects with Default Constructor
///@image html Linear.jpg
///@image html NonLinear.jpg
///@Author <a href="https://bitbucket.org/as004201/">Bogush Danil Sergeevich</a>

int main(int argc, char* argv) {
	LinearEquation 
		///@brief create object of LinearEquation  with Default Constructor
		DefaultLinear,
		///@brief create object of LinearEquation with Constructor N=5
		Linear1(5),
		///@brief create object of LinearEquation with Constructor N=5 Ut=10
		Linear2(5, 10);
	NonLinearEquation 
		///@brief create object of NonLinearEquation with Default Constructor
		DefaultNonLinear,
		///@brief create object of NonLinearEquation with Constructor N=5
		NonLinear1(5),
		///@brief create object of NonLinearEquation with Constructor N=5 Ut=10
		NonLinear2(5, 10);
	std::cout << "Default Constructor\n\n";
	DefaultLinear.show();
	DefaultNonLinear.show();
	std::cout << "N=5\n\n";
	Linear1.show();
	NonLinear1.show();
	std::cout << "N=5, Ut=10\n\n";
	Linear2.show();
	NonLinear2.show();
	return 0;
}

