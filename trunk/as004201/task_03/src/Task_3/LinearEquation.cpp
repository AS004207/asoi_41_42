#include "LinearEquation.h"

void LinearEquation::fill() {
	///@brief calculating the linear model
	for (int i = 0; i < NofY; i++)
		this->Y[i] = 0.988*(i>0? this->Y[i - 1]:0) + 0.232*(*this->Ut);
}

void LinearEquation::show() {
	std::cout << "Linear method" << std::endl  << std::endl;
	if (Y[0] < 0)
		this->fill();
	for (int i = 0; i < NofY; i++)
		std::cout << "Y[" << i + 1 << "]" << "\t=\t" << this->Y[i] << std::endl;
	std::cout << std::endl;
}