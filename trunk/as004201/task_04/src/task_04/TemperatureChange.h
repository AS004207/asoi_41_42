#pragma once
#include <iostream>

///@brief abstract class

class TemperatureChange
{
private:
	double 
		T0 = 0.4,
		Td = 0.01, 
		T = 0.85, 
		K = 0.5,
		q0, q1, q2,
		e1 = 0, e2 = 0, e3 = 0,
		w = 20
		;
protected:
	int NofY;
	double *Y;
	double *Ut;
public:
	///@show virtual method
	virtual void show() = 0;
	///@brief method for work with PID
	void pid(int i) {
		q0 = K*(1 + Td / T0);
		q1 = -K*(1 + 2 * (Td / T0) - T0 / T);
		q2 = K*(Td / T0);
		e3 = e2;
		e2 = e1;
		e1 = w - Y[i];
		(*Ut) += q0*e1 + q1*e2 + q2*e3;
	}
	///@show Constructor
	TemperatureChange(int N=10,int U=10) {
		NofY = N;
		Ut = new double;
		*Ut = U;
		Y = new double[NofY];
		Y[0] = -1;
	}
	///@show Destructor
	~TemperatureChange()
	{
		delete Ut;
		if (Y != NULL)
			delete[NofY]Y;
	}
};

