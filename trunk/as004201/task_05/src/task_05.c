///@mainpage  Hello World
///@image html Result.jpg
///@file task_05.c
///@author Bohush Danya Sergeevich
///@date 09.12.2016

#include <stdio.h>
#include <stdlib.h>
#include "7188.h"

///@brief The program will display on the seven-segment LED the string "HELLO WOrld"
void main()
{
	char ch = 1;
	while(ch!=27)
	{
		Show5DigitLedSeg(1, 0);
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 55);
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 55);
		Show5DigitLedSeg(5, 79);
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 55);
		Show5DigitLedSeg(4, 79);
		Show5DigitLedSeg(5, 14);
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);
		Show5DigitLedSeg(2, 55);
		Show5DigitLedSeg(3, 79);
		Show5DigitLedSeg(4, 14);
		Show5DigitLedSeg(5, 14);
		DelayMs(1000);
		Show5DigitLedSeg(1, 55);
		Show5DigitLedSeg(2, 79);
		Show5DigitLedSeg(3, 14);
		Show5DigitLedSeg(4, 14);
		Show5DigitLedSeg(5, 126);
		DelayMs(1000);
		Show5DigitLedSeg(1, 79);
		Show5DigitLedSeg(2, 14);
		Show5DigitLedSeg(3, 14);
		Show5DigitLedSeg(4, 126);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 14);
		Show5DigitLedSeg(2, 14);
		Show5DigitLedSeg(3, 126);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 30);
		DelayMs(1000);
		Show5DigitLedSeg(1, 14);
		Show5DigitLedSeg(2, 126);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 30);
		Show5DigitLedSeg(5, 60);
		DelayMs(1000);
		Show5DigitLedSeg(1, 126);
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 30);
		Show5DigitLedSeg(4, 60);
		Show5DigitLedSeg(5, 126);
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);
		Show5DigitLedSeg(2, 30);
		Show5DigitLedSeg(3, 60);
		Show5DigitLedSeg(4, 126);
		Show5DigitLedSeg(5, 70);
		DelayMs(1000);
		Show5DigitLedSeg(1, 30);
		Show5DigitLedSeg(2, 60);
		Show5DigitLedSeg(3, 126);
		Show5DigitLedSeg(4, 70);
		Show5DigitLedSeg(5, 14);
		DelayMs(1000);
		Show5DigitLedSeg(1, 60);
		Show5DigitLedSeg(2, 126);
		Show5DigitLedSeg(3, 70);
		Show5DigitLedSeg(4, 14);
		Show5DigitLedSeg(5, 61);
		DelayMs(1000);
		Show5DigitLedSeg(1, 126);
		Show5DigitLedSeg(2, 70);
		Show5DigitLedSeg(3, 14);
		Show5DigitLedSeg(4, 61);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 70);
		Show5DigitLedSeg(2, 14);
		Show5DigitLedSeg(3, 61);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 14);
		Show5DigitLedSeg(2, 61);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 61);
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		ch = _getch();
	}
}
