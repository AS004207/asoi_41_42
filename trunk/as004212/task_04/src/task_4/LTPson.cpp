#include "LTPson.h"
#include "head.h"

using namespace std;
void LinearTemperatureChange::show()///line class
{
	ofstream outText("D:\\linear_result.txt", ios::out);
	if (!outText.is_open())///If you can not open
	{
		cout << "Error" << endl;
		system("pause");
		exit(1);///completion of the program
	}
	cout << "Linear change:" << endl;
	cout << "Y          | T" << endl;
	double Y_line = 0.0;
	Y_line = 0;///first element
	cout.width(10);//widtch = 10
	for (int i = 0; i < 100; i++)
	{
		PID();
		cout.width(10);
		Y_line = 0.988*y + 0.232*u;///Equation of Manuals
		y = Y_line;
		cout << left << Y_line << " | " << u << endl;
		outText << left << i << "\t" << Y_line << endl;
	}
	outText.close();
};
