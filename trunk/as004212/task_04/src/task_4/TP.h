#ifndef _TP_H_
#define _TP_H_

class TemperatureChange
{
public:
	///@param parameters for function PID
	///time step
	double T0 = 0.5;
	///Permanent differentiation
	double Td = 0.01;
	///constant of integration
	double T = 0.85;
	///transfer Ratio
	double k = 0.5;
	///The controller parameters:
	double q0;
	double q1;
	double q2;
	///The deviation of the output variable y (t) from a desired value (t).
	double e1 = 0, e2 = 0, e3 = 0;
	///control action
	double u = 0;
	///The desired value
	double w = 30;
	///The output variable
	double y = 0;
	void PID()
	{
		q0 = k*(1 + (Td / T0));
		q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = k * (Td / T0);
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += (q0 * e1 + q1 * e2 + q2 * e3);
	}
	TemperatureChange();
protected:
	virtual void show() = 0;///virtual method show
};
#endif