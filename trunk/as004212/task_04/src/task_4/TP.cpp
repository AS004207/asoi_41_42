#include "TP.h"

using namespace std;

TemperatureChange::TemperatureChange(){
	///@param parameters for function PID
	///time step
	double T0 = 0.5;
	///Permanent differentiation
	double Td = 0.01;
	///constant of integration
	double T = 0.85;
	///transfer Ratio
	double k = 0.5;
	///The controller parameters:
	double q0;
	double q1;
	double q2;
	///The deviation of the output variable y (t) from a desired value (t).
	double e1 = 0, e2 = 0, e3 = 0;
	///control action
	double u = 0;
	///The desired value
	double w = 30;
	///The output variable
	double y = 0;
}