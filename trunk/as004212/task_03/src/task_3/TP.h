#ifndef _TP_H_
#define _TP_H_

class TemperatureChange
{
public:
	double Y[20];
	double Ut = 10;
	TemperatureChange();
protected:
	
	///@brief Virtual function of the class, which will show the results of modeling.
	virtual void show() = 0;
};
#endif