/**
\file HelloWorld.cpp
\brief Program "Hello world".
 It's simple program which write on console phrase "Hello world".
\author Durpoladow Kuwandyk group: AC-41
*/

#include<iostream>
#include<conio.h>

/**
\fn int main()
Function don't take any parametheres and return 0 when program exit with success.
*/

int main(){

///Out phrase "Hello world on console

	std::cout << "Hello world";

///Wait for input

	_getch();

///Exit program
	return 0;
}