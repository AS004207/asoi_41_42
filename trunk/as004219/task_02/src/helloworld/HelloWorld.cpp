/*! \mainpage ������ "Hello, world!"
* ���������, ������� ������� �� ���������� ������ (�������) ������� "Hello, world!" 
* \file HelloWorld.cpp
* \brief ���� � ����� �� ����� C++, ������� ������� �� ���������� ������ (�������) ������� "Hello, world!" 
*/

#include "stdafx.h"
#include <iostream>
using namespace std;
int main(){ //! �������� ������� ���������. ����� �������������� ����� ������� �� �������.{
    /*!
    ����� ������� �� �����:
    \code
    cout << "Hello, world!" << endl;
    \endcode
    */
  cout << "Hello, world!" << endl;
    /*!
    ������ ��������� �� ����� ����� ����������� ���������� �������:
    \code
    system("pause");
    \endcode
    */
  system("pause");
    /*!
    ��� �������� ���������� ��������� ������� main ���������� ����:
    \code
    return 0;
    \endcode
    */ 
  return 0;
}
