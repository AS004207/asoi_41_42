/// Declaration of the Linear class
 
#ifndef LinearModel_H
#define LinearModel_H

#include"temperature.h"
 
 ///inherits from abstract class TemperatureSystem
class Linear : public TemperatureSystem
{
public:
	///method for creating and displaying parametrs
	void show();
};
#endif