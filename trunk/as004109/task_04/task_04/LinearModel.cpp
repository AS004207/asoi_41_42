///Implements Linear class

#include "temperature.h"
 #include "LinearModel.h"
 #include <iostream>
 #include <fstream>
 using namespace std;

 void Linear::show()
 {
	 ofstream fout("linear.txt", ios::out);
	 cout << "Linear:" << endl;
	 cout << "y" << "\t" << "u" << "\t" << "t" << endl;
	 for (int t = 0; t < 30; t++)
	 {

		 PidController();
		 y1 = 0.988*y + 0.232*u;
		 y = y1;

		 cout << t << "\t" << y1 << "\t" << u << endl;
		 /// writing output to file
		 fout << t << "\t" << y1 << "\t" << u << endl;
	 }
 }