 /// Declaration of the TemperatureSystem class
#ifndef temperature_H
#define temperature_H
class TemperatureSystem
{
public:
	///pure virtual function
	virtual void show() = 0;

	///calculating parametrs of PID-cotroller
	void PidController();

	///parameters for PID-controller
	double T0 = 0.5;
	double Td = 0.01;
	double T = 0.9;
	double K = 0.5;
	double q0 = 0;
	double q1 = 0;
	double q2 = 0;
	double e1 = 0;
	double e2 = 0;
	double e3 = 0;
	double w = 20;
	double u = 0;
	double y = 0;
	double y1 = 0;
	double y2 = 0;
};
#endif