///Declaration of the NonLinear class. 
#ifndef NonLinearModel_H
#define NonLinearModel_H


 #include "temperature.h"

///Derived class
class NonLinear : public TemperatureSystem
{
public:

	void show();
};

#endif