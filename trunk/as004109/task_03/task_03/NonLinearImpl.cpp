///implemention of class NLinear
#include"NonLinear.h"

NLinear::NLinear() :Model()
{
	y = 0;
	yprev = 0;
	ynxt = 0;
}

double NLinear::Calculate()
{
	ynxt = 0.9*y - 0.001*pow(yprev, 2) + u + sin(u);
	yprev = y;
	y = ynxt;
	return ynxt;
}