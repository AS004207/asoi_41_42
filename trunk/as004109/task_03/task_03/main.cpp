#include<iostream>
#include"Model.h"
#include"Linear.h"
#include"NonLinear.h"

using namespace std;

///@mainpage Model of Temperature
///@image html lin1.png
///@image html nonlinear.png

//!main function is the entry point to program
int main()
{
	Model *models[2];
	models[0] = new Linear();
	models[1] = new NLinear();
	///shows result
	for (int i = 0; i < 2; i++)
	{
		models[i]->ImplementModel();
		models[i]->ShowResult();
	}

}