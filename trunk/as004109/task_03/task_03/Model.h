///describes all components of class Model
#ifndef MODEL_H
#define MODEL_H

#include<iostream>
#include<map>

class Model
{
protected:
	const int u = 23;
	std::map<double, double> Map;
	virtual double Calculate() = 0;
public:
	void ImplementModel();
	void ShowResult();
};


#endif