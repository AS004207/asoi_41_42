///implemention of class Model
#include"Model.h"
#include<cmath>
#include <fstream>

using namespace std;

void Model::ImplementModel()
{
	for (int i = 0; i < 40; i++)
	{
		Map.insert(pair<double, double>(i, this->Calculate()));
	}
}

void Model::ShowResult()
{
	ofstream fout("result.txt", ios_base::app); ///creates object of class ofstream for writing to file
	fout << "\nValues x and y:\n\n";
	for (auto it = Map.begin(); it != Map.end(); ++it)
	{
		fout << it->first << ";" << it->second << endl;
	}
	fout.close();
}
