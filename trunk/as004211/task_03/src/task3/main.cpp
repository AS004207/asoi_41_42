#include <math.h>
#include <iostream>

using namespace std;

class ControlObject
{
protected:
	double y0;
public:
	ControlObject()
	{
		y0 = 0;
	}
	double getY0()
	{
		return y0;
	}

	void setY0(double y0)
	{
		this->y0 = y0;
	}
	virtual void showTemperature(int(*u)(int)) = 0;
};

class Object1 : public ControlObject
{
public:

	void showTemperature(int(*ut)(int))
	{
		cout.width(8);
		cout << "y" << " | " << "u" << "   | " << "t" << endl;
		cout << "--------------------" << endl;
		cout.width(8);
		cout << y0 << " | " << ut(0) << " | " << "0" << endl;
		double y = y0;
		for (int i = 1; i <= 10; i++)
		{
			cout.width(8);
			cout << (y = 0.988 * y + 0.232 * ut(i - 1)) << " | " << ut(i - 1) << " | " << i << endl;
		}
	}
};

class Object2 : public ControlObject
{
protected:
	double y1;
public:
	Object2()
	{
		y1 = 20;
	}

	double getY1()
	{
		return y1;
	}

	void setY1(double y1)
	{
		this->y1 = y1;
	}

	void showTemperature(int(*ut)(int))
	{
		cout.width(8);
		cout << "y" << " | " << "u" << "   | " << "t" << endl;
		cout << "--------------------" << endl;
		cout.width(8);
		cout << y0 << " | " << ut(0) << " | " << "0" << endl;
		cout.width(8);
		double yLast = y0, yNext = y1;
		for (int i = 1; i <= 10; i++)
		{
			double y = yNext;
			cout.width(8);
			cout << (yNext = 0.9 * yNext - 0.001 * yLast * yLast + ut(i - 1) + sin(static_cast<double>(ut(i - 2))))
				<< " | " << ut(i - 1) << " | " << i << endl;
			yLast = y;
		}
	}
};

int u(int t)
{
	return 100;
}

int main()
{
	Object1 o1;
	Object2 o2;
	o1.showTemperature(&u);
	cout << endl << endl;
	o2.showTemperature(&u);
	system("pause");
}