///@file 
///@mainpage  Hello World
///@image html Hello.jpg
///@image html world.jpg
///@author Larionov Alexsandr
///@date 15.12.2016

#include"I7188.h"

///@brief The program will display on the seven-segment LED the string "HELLO WORLD"

void main()
{
while(1)
{
Show5DigitLedSeg(5,126);///O
Show5DigitLedSeg(4,55); ///H
Show5DigitLedSeg(3,78); ///C
Show5DigitLedSeg(2,79); ///E
Show5DigitLedSeg(1,0);  ///_
DelayMs(1000);
Show5DigitLedSeg(5,55); ///H
Show5DigitLedSeg(4,78); ///C
Show5DigitLedSeg(3,79); ///E
Show5DigitLedSeg(2,0);  ///_
Show5DigitLedSeg(1,61); ///D
DelayMs(1000);
Show5DigitLedSeg(5,78); ///C
Show5DigitLedSeg(4,79); ///E
Show5DigitLedSeg(3,0);  ///_
Show5DigitLedSeg(2,61); ///D
Show5DigitLedSeg(1,14); ///L
DelayMs(1000);
Show5DigitLedSeg(5,79); ///E
Show5DigitLedSeg(4,0);  ///_
Show5DigitLedSeg(3,61); ///D
Show5DigitLedSeg(2,14); ///L
Show5DigitLedSeg(1,70); ///R
DelayMs(1000);
Show5DigitLedSeg(5,0);  ///_
Show5DigitLedSeg(4,61); ///D
Show5DigitLedSeg(3,14); ///L
Show5DigitLedSeg(2,70); ///R
Show5DigitLedSeg(1,126);///O
DelayMs(1000);
Show5DigitLedSeg(5,61); ///D
Show5DigitLedSeg(4,14); ///L
Show5DigitLedSeg(3,70); ///R
Show5DigitLedSeg(2,126);///O
Show5DigitLedSeg(1,62); ///W
DelayMs(1000);
Show5DigitLedSeg(5,14); ///L
Show5DigitLedSeg(4,70); ///R
Show5DigitLedSeg(3,126);///O
Show5DigitLedSeg(2,62); ///W
Show5DigitLedSeg(1,0);  ///_
DelayMs(1000);
Show5DigitLedSeg(5,70); ///R
Show5DigitLedSeg(4,126);///O
Show5DigitLedSeg(3,62); ///W
Show5DigitLedSeg(2,0);  ///_
Show5DigitLedSeg(1,126);///O
DelayMs(1000);
Show5DigitLedSeg(5,126);///O
Show5DigitLedSeg(4,62); ///W
Show5DigitLedSeg(3,0);  ///_
Show5DigitLedSeg(2,126);///O
Show5DigitLedSeg(1,14); ///L
DelayMs(1000);
Show5DigitLedSeg(5,62); ///W
Show5DigitLedSeg(4,0);  ///_
Show5DigitLedSeg(3,126);///O
Show5DigitLedSeg(2,14); ///L
Show5DigitLedSeg(1,14); ///L
DelayMs(1000);
Show5DigitLedSeg(5,0);  ///_
Show5DigitLedSeg(4,126);///O
Show5DigitLedSeg(3,14); ///L
Show5DigitLedSeg(2,14); ///L
Show5DigitLedSeg(1,79); ///E
DelayMs(1000);
Show5DigitLedSeg(5,126);///O
Show5DigitLedSeg(4,14); ///L
Show5DigitLedSeg(3,14); ///L
Show5DigitLedSeg(2,79); ///E
Show5DigitLedSeg(1,55); ///H
DelayMs(1000);
}
}