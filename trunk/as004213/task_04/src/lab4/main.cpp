///@file
///@author Larionov Alexsandr
///@date 18.11.2016
///@mainpage PID controller
///@image html linear.jpg
///@details Graphic of linear temperature changes.
///@image html nonlin.jpg
///@details Graphic of nonlinear temperature changes.
///@brief Looking at the chart, you will notice that the linear model is adjusted more slowly than non - linear


#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include "TempChange.h"
#include "Linear.h"
#include "NonLinear.h"

using namespace std;
int main()
{
	Linear line;
	NonLinear NoLine;
	line.show();
	NoLine.show();
	system("pause");
	return 0;
}
