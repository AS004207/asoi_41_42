///@file
#pragma once
#include <iostream>
///@brief abstract class	
class model
{
private:
	double array[15] = {};
public:
	model() :array() {};
	virtual void ShowResult() = 0;

};

///@class Lin
///@brief Class Lin derived from Abs
class Lin :public model
{
private:
	double array[15];
public:
	Lin() :array() {}
	///@brief Lin method solution 
	void ShowResult();
};

///@class Nlin
///@brief Class Nlin derived from Abs 
class Nlin : public model
{
private:
	double array[15] = {};
public:
	Nlin() :array() {}

	///@brief Nlin method solution 
	void ShowResult();
};
