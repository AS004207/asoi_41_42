﻿///@File
///@Author Пархач Дмитрий
///@25.10.2016
#include <iostream> 
#include <math.h>
#include "model.h"
using namespace std;
///@param Input temperature 
const double ut = 10;

///@mainpage Model of Temperature 
///@image html ln.png
///@image html nln.png
int main()
{
	Lin *a = new Lin;
	Nlin *b = new Nlin;
	a->ShowResult();
	b->ShowResult();
	system("pause");
	return 0;
}