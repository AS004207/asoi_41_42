///@file
#include "model.h"
#include <iostream>
#include <math.h> 
using namespace std;
extern double ut= 10;

///@brief Method for the linear model
void Lin::ShowResult()
{
  for (int i = 1; i <= 14; i++)
    {
	  ///@brief the formula for calculating the linear model		
	  array[i] = 0.988*array[i - 1] + 0.232*ut;
    }
///@brief Lin output 
  cout << "Lin solution: " << endl;
  for (int i = 1; i <= 14; i++)
    {
	  cout << "t=" << i << " y=" << array[i] << endl;
    }
}

///@brief Method for the notlinear model
void Nlin::ShowResult() {
	for (int i = 1; i <= 14; i++)
	{
		if (i > 1)
		{
			///@brief the formula for calculating the notlinear model
			array[i] = 0.9*array[i - 1] - 0.001*array[i - 2] * array[i - 2] + ut + sin(ut);
		}
	}

	///@brief Nlin output 
	cout << "\nNlin solution: " << endl;
	for (int i = 1; i <= 14; i++)
	{
		cout << "t=" << i << " y=" << array[i] << endl;
	}
}
