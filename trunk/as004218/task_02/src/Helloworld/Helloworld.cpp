﻿/**
\file
\brief Программа "Hello, world!". Исходный код.
\authors Пархач Дмитрий Сергеевич, группа: АС-42.
\date 23.10.2016

Программа, написанная на объектно-ориентированном языке C++.

*/
#include <iostream>

/**
Основная функция 'main', выполняет вывод в
консоль сообщения: "Hello, world!".
\return Нулевое значение.
*/
int main() {
	std::cout << "Hello, world!" << std::endl;
	return 0;
}