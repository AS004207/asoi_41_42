﻿#pragma once
/**
@mainpage Model of Temperature 
//@brief Линейная модель
@image html Linear.png
//@brief Нeлинейная модель
@image html Nonlinear.png
Из графиков видно что мы регулируем модели.
Нелинейная модель регулируется быстрее чем линейная.
*/
class GenModel
{
public:
	GenModel();
	///@brief абстрактный метод для вывода значений моделей
	virtual double Model(double yt, double ut) = 0;
	~GenModel();
};

