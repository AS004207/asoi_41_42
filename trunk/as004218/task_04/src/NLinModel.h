﻿#pragma once
#include "GenModel.h"
class NlinModel :
	public GenModel
{
private:
	double ytPrev;
public:
	NlinModel();
	///@brief задает входное значение интенсивтости и начальное входное значение температуры
	///@param y0 - начальное входное значение температуры
	NlinModel(double y0);
	///@brief выводит на консоль входное значение температуры на каждой итерации(Нелинейная модель)
	///@param yt - начальное входное значение температуры
	///@param ut - входное значение интенсивтости
	double Model(double yt, double ut);
	~NlinModel();
};
