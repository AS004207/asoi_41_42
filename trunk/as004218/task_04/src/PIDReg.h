﻿#pragma once
///@brief  класс, регулирующий выходную температуру
class PIDReg
{
private:
	double q0, q1, q2;
	double T0, Td, T;
	double K;
	double eFirst, eSec, eThird;
	double u;

public:
	PIDReg();
	PIDReg(double Td, double T0, double T, double K);
	/// высчитывает ut
	///@param y -  управл€ющее возздействие
	///@param w - алгоритм функционировани€ системы
	///@return - ut
	double Deviation(double y, double w);
	~PIDReg();
};