///@file
#pragma once
#include "parent.h"

///@class nonlinear 
///@class  nonlinearl derived from parent
class nonlinear : public parent
{
public:
	///@brief Method for the nonlinear model
	void show();
};