///@file
#include "nonlinear.h"
#include <iostream>
using namespace std;
extern double u;
///@brief Method for the notlinear model
void nonlinear::show()
{
	cout << "\tNonlinear model " << endl << endl;
	cout.width(10);
	cout << "y" << "\t" << "u" << "\t" << "t" << endl << endl;
	y[0] = 0;
	y[1] = 0;
	for (int t = 1; t < 11; t++)
	{
		///@brief the formula for calculating the nonlinear model
		y[t + 1] = 0.9*y[t] - 0.001*y[t - 1] * y[t - 1] + u + sin(u);
		cout.width(10);
		cout << y[t] << "\t" << u << "\t" << t << endl << endl;
	}
}