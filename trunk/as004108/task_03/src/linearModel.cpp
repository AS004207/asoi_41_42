///@file
#include "linear.h"
#include <iostream>
using namespace std;
extern double u;
void linear::show()
{
	cout << "\tLinear model" << endl << endl;
	cout.width(10);
	cout << "y" << "\t" << "u" << "\t" << "t" << endl << endl;
	y[0] = 0;
	for (int t = 0; t < 11; t++)
	{
		///@brief the formula for calculating the linear model
		y[t + 1] = 0.988*y[t] + 0.232*u;
		cout.width(10);
		cout << y[t] << "\t" << u << "\t" << t << endl << endl;
	}
}