﻿///@file Linear.h  содержит описание для линейной модели

#pragma once
#include "Abstract.h"
#ifndef LINEAR_H
#define LINEAR_H

class Linear : public Abstract {
public:
	///@briefКонструктор по умолчанию
	Linear();
	///@briefДеструктор
	~Linear();
	///@brief Метод, который выводит информацию о выходной температуре
	void output();
};

#endif