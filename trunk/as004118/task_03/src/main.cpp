﻿///@file main.cpp содержит главную функцию программы
///@author Марчук Роман

#include "stdafx.h"
#include <iostream>
#include <math.h>
#include "Linear.h"
#include "Nonlinear.h"

/**@mainpage Simulation of the control object
\image html nonlinear.png
\details Зависимость представленная на графике является нелинейной т.к. присутствуют элементы, имеющие нелинейную зависимость
\image html linear.png
\details Зависимость представленная на графике является линейной т.к. нет элементов имеющих нелинейную зависимость
*/

int main(int argc, char* argv[]) {
	Linear a;
	Nonlinear b;
	a.output();
	b.output();
	return 0;
};