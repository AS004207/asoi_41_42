///@file Nonlinear.h сcontains a Nonlinear class description
#pragma once
#include "Abstract.h"
#ifndef NONLINEAR_H
#define NONLINEAR_H

/**@class Nonlinear 
Nonlinear class inherited from Abstract class
*/

class Nonlinear : public Abstract {
public:
	///@brief Default constructor
	Nonlinear();
	///@brief Destructor
	~Nonlinear();
	///@brief Output method for nonlinear model
	void output();
};

#endif