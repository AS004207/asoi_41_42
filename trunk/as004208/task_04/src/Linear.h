#pragma once
#include "Parent.h"
#include <iostream>

///@class Linear
///@brief Class Linear derived from Parent
class Linear : public Parent
{
public:
	void Linear::ShowResult();
};
