#include "Parent.h"
#include "Nonlinear.h"
using namespace::std;

///@brief Nonlinear solution
void NonLinear::ShowResult()
{
	for (int i = 1; i <= 20; i++)
	{
		PidController();
		y2 = 0.9*y1 - 0.001*y * y + u + sin(u);
		y1 = y2;
		y = y1;
		///@brief Nonlinear output
		cout << i << "  |  " << y2 << "  |  " << u << endl;
	}
}