#pragma once
#include <iostream>
#include "Parent.h"

///@class NonLinear
///@brief Class NonLinear derived from Parent
class NonLinear : public Parent
{
public:
	void NonLinear::ShowResult();
};
