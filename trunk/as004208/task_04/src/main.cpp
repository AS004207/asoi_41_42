///@File
///@Author 
///@Date 10.11.2016
#include <iostream>
#include "Parent.h"
#include "Linear.h"
#include "Nonlinear.h"
using namespace::std;

///@mainpage Graphs
///@image html Linear.png 
///@image html Nonlinear.png
int main()
{
	setlocale(0, "");
	Linear *ObjectLinear = new Linear;
	NonLinear *ObjectNonLinear = new NonLinear;
	cout << "Linear result" << endl;
	cout << "           Y" << "  |  " << "      U" << "  |  " << endl;
	ObjectLinear->ShowResult();
	cout << "Nonlinear result" << endl;
	cout << "           Y" << "  |  " << "      U" << "  |  " << endl;
	ObjectNonLinear->ShowResult();
	system("pause");
	return 0;
}