#pragma once
#include "Parent.h"
using namespace::std;

///@class Linear
///@brief Class Linear derived from Parent 
class Linear :public Parent
{
private:
	double array_y[11];
public:
	Linear() :array_y() {}

	///@brief Linear method solution 
	void ShowResult();
};
