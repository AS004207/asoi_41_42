/** \file main.cpp
 *  \author Юхно Владислав (as004224)
 *  \brief содежрит ф-цию main
*/

#include <iostream>
#include <cstring>
#include "models.h"

#define INPSIZE 12


/**
 *
 * \image html scr01.png
 * \image html scr02.png
 * @param argc кол-во аргументов переданные программе, включа путь
 * @param argv массив строк содержащий эти аргументы
 * @return 0
 */
int main(int argc, char **argv){

  LinModel lm;
  NonLinModel nlm;

  
  for(int i = 0; i < INPSIZE; i++)
    lm.append_inp(24);

  if(argc > 1 && strcmp(argv[1],"ln") == 0 ){
    lm.calculate();
    lm.print();
  }else if(argc > 1 && strcmp(argv[1],"nln") == 0){
    nlm.set_inp(lm.get_inp());
    nlm.calculate();
    nlm.print();
  }
  else{
    fprintf(stderr, "First arg must be 'ln' or 'nln' \n");
  }
  return 0;
}
