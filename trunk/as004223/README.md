# REQUIREMENTS
1. c++ compiler
2. cmake
# INSTALLATION
1. `git clone`
2. `cd trunk/as004223 & mkdir build`
3. `cmake -H. -Bbuild -DCMAKE_BUILD_TYPE=Release`
4. `cmake --build build`
5. `cd build & ls -la`