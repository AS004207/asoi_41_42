## Настройка среды разработки.
Разработка программы "Hello World" для контроллера PAC ICPCON 7188D будет вестись из DOSBox.
1. Загрузим и установим DOSBox.

   * Windows: [DOSBox0.74-win32-installer.exe](https://sourceforge.net/projects/dosbox/files/dosbox/0.74/DOSBox0.74-win32-installer.exe/download).  
   * GNU/Linux: искать в репозиториях вашего дистрибутива.  
   В моем случае: ```yaourt -S dosbox``` 

2. [ОПЦИОНАЛЬНО] Избавимся от рутинного монтирования каталога л.р. добавив автомонтирование в config файл DOSBox.  
   ```echo "mount e /home/jaffee/JB/proj/asoi_41_42/trunk/as004223/task_05" >> ~/.dosbox/dosbox-0.74.conf```  
   Второй аргумент mount - путь к каталогу монтирования.  
   
   Теперь при запуске DOSBox будет происходить автомонтирование каталога л.р. как раздел E:\  

3. Согласно [инструкции](http://f.icp-das.ru/files/add/doc/224792/compiler.pdf) установим компилятор TC 2.01 и добавим в переменную PATH путь к TC.exe.  
   У меня получился следующий конфигурационный файл.
   ```
   [autoexec]  
   mount s /home/jaffee  
   mount e /home/jaffee/JB/proj/asoi_41_42/trunk/as004223/task_05  
   s:  
   path s:\TC  
   e:  
   cls  
   ```

## Разрешение зависимостей.
Для сборки проекта нам понадобится статическая библиотека **I7188S.LIB** и заголовочный файл **I7188.h**.
1. Загрузим [DEMO(i7188.exe)](http://www.icpdas.com/download/i7188/i7188.exe) с оффициального сайта нашего контроллера, где и будут лежать требуемые файлы.
2. Запустим i7188.exe и извлечем файлы в удобное место.  
   ![alt text](./img/demo_exp.png "EXPORT DEMO PROJECTS")  
3. Скопируем **I7188S.LIB** из `.../i7188/MINIOS7/DEMO/LIB/` в `.../TC/LIB/`  
4. Скопируем **I7188.h** из `.../i7188/MINIOS7/DEMO/LIB/` в `.../TC/INCLUDE/`  
5. Создадим HW.RPJ файл в `.../trunk/as004223/task_05/src` следующего содержания:  
   ```
   HW.C  
   I7188S.lib  
   ```  
После выполненных действий можно приступать к написанию программы "Hello World" и ее сборке.  

## Сборка проекта.  
1. ```dosbox &```
2. ```cd SRC```, ```tc```
4. Откроем ранее созданный проект  
   ```Project -> Project name -> HW.PRJ```
3. Проверим выставленную модель памяти и установленный набор инструкций процессора.  
   ![alt text](./img/check.png "") 
4. Соберем проект  
   ```Compile -> Build all ```  
   ![alt text](./img/suc.png "") 

## Видео работы программы  

   [![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/Jehh0gkghAs/0.jpg)](https://youtu.be/Jehh0gkghAs)  