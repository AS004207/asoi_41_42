/** \file helloworld.cpp
 *  Contains main function
 */ 
#include <iostream>

/**
 *  "main" function which is the "start point" of each c++ program;
 *  \param[in] argc The number of a program arguments, including path program;
 *  \param[in] argv The string array which contains arguments of a program;
 *  \returns integer which describes how program was finished;
 */
int main(int argc, char** argv){
  /// print in stdout a 'hello' string;
  std::cout << "Hello world!\n"; 
  return 0;
}
