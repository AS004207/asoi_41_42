/** \file TemperatureSystem.h
*  \author Karpovich Vladislav (as004111)
*  \brief Declaration of the TemperatureSystem class.
*/
#pragma once
#include<iostream>

///@brief Abstract class TemperatureSystem
class TemperatureSystem
{
double
		T0 = 0.2,
		Td = 0.01,
		T = 0.5,
		K = 0.5,
		q0, q1, q2,
		e1 = 0, e2 = 0, e3 = 0,
		w = 20;
protected:
	///@brief Declare variables to store input/output values.
	double systemInputValue;
	double systemOutputValue;
	int numIteration;
public:
	///@brief Declare constructor with paramethers.
	TemperatureSystem(double, double, int);
	///@brief Virtual function to calculate temperature.
	virtual void calculate() = 0;
	void pid();
};

