/** \file Main.cpp
*  \author Karpovich Vladislav (as004111)
*  \brief Consist main function.From this function program begin executing.
*/


#include<iostream>
#include"LinearModel.h"
#include"NonlinearModel.h"

using namespace std;

/**
* \mainpage
* \image html linearModel.png
* \image html nonlinearModel.png
* \image html program.png
*/

int main()
{
	///@brief Create linear and nonlinear objects.
	LinearModel linear = LinearModel(20, 0,6);
	NonlinearModel nonlinear = NonlinearModel(20, 0,6);

	cout << "Linear model" << endl;
	linear.calculate();
	cout << endl << "NonLinear model" << endl;
	nonlinear.calculate();

	system("pause");
	return 0;
}