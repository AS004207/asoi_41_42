/** \file NonlinearModel.h
*  \author Karpovich Vladislav (as004111)
*  \brief Declaration of the NonlinearModel class.
*/

#pragma once
#include "TemperatureSystem.h"

///@brief Class NonlinearModel inherit from abstract class TemperatureSystem.
class NonlinearModel :
	public TemperatureSystem
{
public:
	///@brief Variable to store previose output value of model.
	double prevValue;
	///@brief Declare constructor with paramethers.
	NonlinearModel(double, double, int);
	///@brief Declare inherited function.
	void calculate();
};

