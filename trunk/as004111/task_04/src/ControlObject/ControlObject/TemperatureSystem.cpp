/**\file TemperatureSystem.cpp
*  \author Karpovich Vladislav (as004111)
*  \brief Implementation of the NTemperatureSystem class.
*/
#include "TemperatureSystem.h"

///@brief Implement abstract class constructor.
TemperatureSystem::TemperatureSystem(double inputValue, double outputValue,int numberIteration)
{
	systemInputValue = inputValue;
	systemOutputValue = outputValue;
	numIteration = numberIteration;
}

///@brief Implement PID regulator
void TemperatureSystem::pid() {
	q0 = K*(1 + Td / T0);
	q1 = -K*(1 + 2 * (Td / T0) - T0 / T);
	q2 = K*(Td / T0);
	e3 = e2;
	e2 = e1;
	e1 = w - systemOutputValue;
	systemInputValue += q0*e1 + q1*e2 + q2*e3;
}