/** \file LinearModel.h
*  \author Karpovich Vladislav (as004111)
*  \brief Declaration of the LinearModel class.
*/

#pragma once
#include "TemperatureSystem.h"

///@brief Class LinearModel inherit from abstract class TemperatureSystem.
class LinearModel :
	public TemperatureSystem
	
{
public:
	///@brief Declare constructor with paramethers.
	LinearModel(double, double);
	///@brief Declare inherited function.
	double calculate();
};

