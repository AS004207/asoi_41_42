/** \file Main.cpp
*  \author Karpovich Vladislav (as004111)
*  \brief Consist main function.From this function program begin executing.
*/


#include<iostream>
#include"LinearModel.h"
#include"NonlinearModel.h"

using namespace std;

/**
* \mainpage
* \image html linearModel.png
* \image html nonlinearModel.png
* \image html program.png
*/

int main()
{
	const int numIteration = 8;
	///@brief Create linear and nonlinear objects.
	LinearModel linear = LinearModel(20, 45);
	NonlinearModel nonlinear = NonlinearModel(20, 45);

	cout << "\nLinear model" << endl << endl;
	for (int i = 0; i < numIteration; i++)
	{
		cout << linear.calculate() << endl;
	}

	cout << "\nNonlinear model" << endl << endl;
	for (int i = 0; i < numIteration; i++)
	{
		cout << nonlinear.calculate() << endl;
	}

	system("pause");
	return 0;
}