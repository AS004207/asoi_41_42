/**\file TemperatureSystem.cpp
*  \author Karpovich Vladislav (as004111)
*  \brief Implementation of the NTemperatureSystem class.
*/
#include "TemperatureSystem.h"

///@brief Implement abstract class constructor.
TemperatureSystem::TemperatureSystem(double inputValue, double outputValue)
{
	systemInputValue = inputValue;
	systemOutputValue = outputValue;
}