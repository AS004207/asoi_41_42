
/** \file
\brief  Contains the description of a class nonline
*/
#include "stdafx.h"
#include "ControlObject.h"
#pragma once

/// The non-linear model of a control object is described. Is a successor of a class ControlObject.
class NonLine : public ControlObject
{
protected:
	double old_U;
	double old_Y;
public:
	/// obtaining result of simulation of non-linear system
	double modeling(double Y, double U);

	NonLine();

	~NonLine() {}
};
