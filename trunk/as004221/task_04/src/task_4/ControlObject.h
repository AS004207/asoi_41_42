
/** \file
\brief Contains the description of the abstract class ControlObject
*/
#pragma once
#include "stdafx.h"
/// The abstract control object is described. Is basic for real control objects.

class ControlObject
{
public:
	virtual double modeling(double, double) = 0;

	ControlObject() {}


};
