
/** \file
\brief  Contains the description of a class pid
*/

#include "stdafx.h"
#include "pid.h"


	
	void pid::calc_q()
	{
		q0 = K* (1 + (Td / T0));
		q1 = -K *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = K * (Td / T0);
	}

	double pid::Calc(double Y, double W)
	{
		e3 = e2;
		e2 = e1;
		e1 = W - Y;
		U += (q0 * e1 + q1 * e2 + q2 * e3);
		return U;
	}

	pid::pid(double T0 , double Td , double T , double K )
	{
		this->T0 = T0;
		this->Td = Td;
		this->T = T;
		this->K = K;
	}

