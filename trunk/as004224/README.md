# Laboratory work AS004224.

If you work under OS Windows, you need:

- [IDE Microsoft Visual Studio 2015 CE] or some other IDE for C++ development ([CLion]);
- C++ compiler (g++, clang++, visual c++ compiler);
- Build system [CMake] (3.6.2 or later);

If you work in some Linux distros, you need:

- C++ compiler (g++, clang++);
- Build system [CMake] (3.6.2 or later);

## Example getting executable file (Linux system):
```sh
$ cd target_directory
$ cmake CMakeLists.txt
$ make
$ cd build
$ ls -l
```

## Example getteng solution for IDE Microsoft Visual Studio 2015 CE:
```sh
$ cd target_directory
$ cmake -G "Visual Studio 14 2015 Win64"
$ dir
$ EXECUTABLE_NAME.sln
```

[CMake]: <https://cmake.org/download/>
[CLion]: <https://www.jetbrains.com/clion/>
[IDE Microsoft Visual Studio 2015 CE]: <https://www.visualstudio.com/ru/downloads/>
