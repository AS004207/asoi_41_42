#ifndef TASK_03_TEMPERATUREMODEL_H
#define TASK_03_TEMPERATUREMODEL_H

#include <iostream>
#include <cmath>
#include <string>

using std::ostream;
using std::cout;
using std::string;

class TemperatureModel{
protected:
    double desired_temperature; 
    unsigned int number_of_calc;
    float k_transmission;
    float t_integration;
    float td_differentiation;
    float t0_time;
    void applyFormat(string &, string &, string &, const bool &);
    void qValuesInitialise(double &, double &, double &);
public:
    TemperatureModel(double, unsigned int);
    void changeStartValue(double);
    void changeCalcNumber(unsigned int);
    void changeTransmissionValue(float);
    void changeIntegrationValue(float);
    void changeDifferentiationValue(float);
    double getStartValue(void);
    unsigned int getCalcNumber(void);
    float getTransmissionValue(void);
    float getIntegrationValue(void);
    float getDifferentiationValue(void);
    virtual void printModel(ostream &, const bool = false) = 0;
};

class LinearModel : public TemperatureModel{
public:
    LinearModel(double, unsigned int = 10);
    virtual void printModel(ostream &, const bool = false);
};

class NonLinearModel : public TemperatureModel{
public:
    NonLinearModel(double, unsigned int = 10);
    virtual void printModel(ostream &, const bool = false);
};

#endif
