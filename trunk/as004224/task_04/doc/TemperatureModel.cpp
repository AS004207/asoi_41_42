#include "TemperatureModel.h"

using std::cout;
using std::cin;
using std::endl;
using std::ostream;
using std::string;

void TemperatureModel::applyFormat(string & _initial_formatting, string & _intermediate_formatting, 
                                    string & _final_formatting, const bool & _file_formatting){
    if (!_file_formatting){
        _initial_formatting = "y[";
        _intermediate_formatting = "] = ";
        _final_formatting = ";";
    }
    else 
        _intermediate_formatting = ";";
}
void TemperatureModel::qValuesInitialise(double & _q_0, double & _q_1, double & _q_2){
    _q_0 = k_transmission * (1 + td_differentiation / t0_time);
    _q_1 = -k_transmission * (1 + 2 * td_differentiation / t0_time - t0_time / t_integration);
    _q_2 = k_transmission * td_differentiation / t0_time;
}
TemperatureModel::TemperatureModel(double _desired_temperature, unsigned int _number_of_calc){
    desired_temperature = _desired_temperature;
    number_of_calc = _number_of_calc;
    k_transmission = 0.1;
    t_integration = 0.1;
    td_differentiation = 0.1;
    t0_time = 0.1;
}
void TemperatureModel::changeStartValue(double _start_value){
    desired_temperature = _start_value;
}
void TemperatureModel::changeCalcNumber(unsigned int _new_calc_number){
    number_of_calc = _new_calc_number;
}
void TemperatureModel::changeTransmissionValue(float _new_transmission_value){
    k_transmission = _new_transmission_value;
}
void TemperatureModel::changeIntegrationValue(float _new_integration_value){
    t_integration = _new_integration_value;
}
void TemperatureModel::changeDifferentiationValue(float _new_differentiation_value){
    td_differentiation = _new_differentiation_value;
}
double TemperatureModel::getStartValue(void){
    return desired_temperature;
}
unsigned int TemperatureModel::getCalcNumber(void){
    return number_of_calc;
}
float TemperatureModel::getTransmissionValue(void){
    return k_transmission;
} 
float TemperatureModel::getIntegrationValue(void){
    return t_integration;
}
float TemperatureModel::getDifferentiationValue(void){
    return td_differentiation;
}
void TemperatureModel::printModel(ostream & _output_target, const bool _file_formatting){
    _output_target << "The desired temperature: " << desired_temperature << ";" << endl <<
    "Number of calculations: " << number_of_calc << ";" << endl;
}

LinearModel::LinearModel(double _desired_temperature, unsigned int _number_of_calc) : 
    TemperatureModel(_desired_temperature, _number_of_calc){}
void LinearModel::printModel(ostream & _output_target, const bool _file_formatting){
    TemperatureModel::printModel(_output_target);
    _output_target << "Linear model table:" << endl;
    
    string initial_formatting, intermediate_formatting, final_formatting;

    applyFormat(initial_formatting, intermediate_formatting, final_formatting, _file_formatting);

    double y {0};
    double u {0};

    double q_0 {0}, q_1 {0}, q_2 {0};
    double ek {0}, ek_1 {0}, ek_2 {0};

    qValuesInitialise(q_0, q_1, q_2);

    for(unsigned int i {1}; i <= number_of_calc; i++){
        _output_target << initial_formatting << i << intermediate_formatting << y << final_formatting << endl;
        y = 0.988 * y + 0.232 * u;
        ek_2 = ek_1;
        ek_1 = ek;
        ek = desired_temperature - y;
        u = u + q_0 * ek + q_1 * ek_1 + q_2 * ek_2;
    }
}

NonLinearModel::NonLinearModel(double _desired_temperature, unsigned int _number_of_calc) :
    TemperatureModel(_desired_temperature, _number_of_calc){}
void NonLinearModel::printModel(ostream & _output_target, const bool _file_formatting){
    TemperatureModel::printModel(_output_target);
    _output_target << "Nonlinear model table:" << endl;

    string initial_formatting, intermediate_formatting, final_formatting;

    applyFormat(initial_formatting, intermediate_formatting, final_formatting, _file_formatting);

    double y_1 {0};
    double y_2 {0};
    double y_3 {0};
    double u {0};
    double u_1{0};

    double q_0 {0}, q_1 {0}, q_2 {0};
    double ek {0}, ek_1 {0}, ek_2 {0};

    qValuesInitialise(q_0, q_1, q_2);

    for(unsigned int i {1}; i <= number_of_calc; i++){
        _output_target << initial_formatting << i << intermediate_formatting << y_3 << final_formatting << endl;
        y_1 = y_2;
        y_2 = y_3;
        y_3 = 0.9 * y_2 - 0.001 * pow(y_1, 2) + u + sin(u_1);
        ek_2 = ek_1;
        ek_1 = ek;
        ek = desired_temperature - y_3;
        u_1 = u;
        u = u + q_0 * ek + q_1 * ek_1 + q_2 * ek_2;
    }
}
