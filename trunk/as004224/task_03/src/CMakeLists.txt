cmake_minimum_required(VERSION 3.6.2)
project(task_03)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp TemperatureModel.h TemperatureModel.cpp )
set(USR_LIB_FILES USRCustomFunction.h USRCustomFunction.cpp)
add_executable(EXECUTABLE ${SOURCE_FILES} ${USR_LIB_FILES})