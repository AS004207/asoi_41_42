/**
\file
\brief Программа моделирования объекта управления на языке C++.
\authors Яцевич Игорь Николаевич, группа АС-42.
\date 21.10.2016
\warning В программе не реализована защита от ввода некорректных
данных, пожалуйста, следите за типом вводимых данных.

Программа, написанная на объекто-ориентированном языке C++.
Система сборки проекта - СMake. Основной целью реализации 
данной программы, является выполнение моделирования объекта 
управления, согласно двум представленным моделям: линейной и 
нелинейной.

Линейная модель:
\f[y_{t+1} = 0.988y_{t} + 0.232u_{t}\f]

Нелинейная модель:
\f[y_{t+1} = 0.9y_{t} - 0.001y^2_{t-1} + u_{t} + sin(u_{t})\f]

В ходе написания данной программы подразумевается использование
ООП, создается абстрактный базовый класс (TemperatureModel), 
имеющий какие-либо общие черты для последующих классов-наследников. 
А именно: входное количество теплоты и количество выходных значений.
После чего, выполняется наследование данного класса, классами,
описывающими линейную (LinearModel) и нелинейную (NonLinearModel)
модели управления.

Пример работы программы. Для линейной и нелинейной модели объекта 
управления зададим начальное значение входного количества теплоты, 
равное 30, и количество выходных значений равным 15.

Заполнение данных о моделях управления:
![Заполнение данных.](enter_data.png)

Пример пользовательского меню в программе:
![Выбор операций.](user_menu.png)

Результат вывода для линейной модели объекта управления:
![Вывод данных линейной модели.](linear_model.png)

График изменения температуры объекта управления для линейной модели 
управления:
![График линейной модели.](linear_model_graph.png)

Результат вывода для нелинейной модели объекта управления:
![Вывод данных нелинейной модели.](nonlinear_model.png)

График изменения температуры объекта управления для нелинейной модели 
управления:
![График линейной модели.](nonlinear_model_graph.png)
*/
#include "TemperatureModel.h"
#include "USRCustomFunction.h"
#include <iostream>

using std::cout;
using std::cin;
using std::endl;

/**
Функция, отвечающая за вывод пользовательского меню и последующие
манипуляции с объектами классов LinearModel и NonLinearModel.
Выводит список доступных пользователю функций, для манипуляций с
моделируемыми моделями. К таким функциям относится:
- Вывод информации о линейной/нелинейной модели управления,
смоделированной с заданными параметрами.
- Установка нового входного количества теплоты для линейной/нелинейной
модели управления.
- Установка нового количества выходных значений для линейной/нелинейной
модели управления.
- Выход из программы.

Осуществлении навигации по пользовательскому меню реализуется при
помощи чтения из потока ввода соттветствующих команд, введенных
пользователем.

\param[in,out] _linear_obj Указатель на объект для манипуляции, 
класса LinearModel.
\param[in,out] _non_linear_obj Указатель на объект для манипуляции,
класса NonLinearModel.
*/
void usermenu(LinearModel *, NonLinearModel *);

/**
Функция, отвечающая за установку нового значения тепла,
поступающего в смоделированную систему. При выполнении данной функции
выводится консольное приглашение на ввод нового значения тепла,
полученное значение связывается с соответствующей моделью.
Тип вводимых данных - double.

\param[in,out] _changed_value Указатель на объект для манипуляции, 
класса TemperatureModel. Реализуется принцип полиморфизма.
*/
void newheatvalue(TemperatureModel *);

/**
Функция, отвечающая за установку нового количества выходных
значений для выбранной модели.При выполнении данной функции
выводится консольное приглашение на ввод нового значения
количества выходных значений,полученное значение связывается 
с соответствующей моделью.
Тип вводимых данных - unsigned int.

\param[in,out] _changed_value Указатель на объект для манипуляции, 
класса TemperatureModel. Реализуется принцип полиморфизма.
*/
void newoutputvalue(TemperatureModel *);

/**
Основная функция 'main' написанной программы, выполняет 
первоначальное чтение данных для построения соответствующих
моделей управления. После успешного чтения даннных вызывает
функцию для вывода пользовательского меню для выбора 
дальнейших действий.

\return Нулевое значение (в случае успешного завершения).
*/
int main(){

    double heat_value_linear;
    unsigned int calc_linear;

    ClearScreen();
    cout << "Enter the amount of heat entering to the system (linear model): ";
    cin >> heat_value_linear;
    cout << "Enter the desired number of output values (linear model): ";
    cin >> calc_linear;
    cout << endl;   

    LinearModel *linear_obj = new LinearModel(heat_value_linear, calc_linear);
    double heat_value_non_linear;
    unsigned int calc_non_linear;

    cout << "Enter the amount of heat entering to the system (nonlinear model): ";
    cin >> heat_value_non_linear;
    cout << "Enter the desired number of output values (nonlinear model): ";
    cin >> calc_non_linear;
    cout << "Data added." << endl;

    NonLinearModel *non_linear_obj = new NonLinearModel(heat_value_non_linear, calc_non_linear); 

    usermenu(linear_obj, non_linear_obj);

    delete linear_obj;
    delete non_linear_obj;
    return 0;
} 

void usermenu(LinearModel *_linear_obj, NonLinearModel *_non_linear_obj){
    char ch;
    while(ch != 'Q' && ch != 'q'){
        cout << endl <<
        "1. Print data of the linear model." << endl << 
        "2. Print data of the nonlinear model." << endl << 
        "3. Set a new value for the amount of heat of the linear model." << endl << 
        "4. Set a new value for the amount of heat of the nonlinear model." << endl << 
        "5. Set the new number of output values for the linear model." << endl << 
        "6. Set the new number of output values for the nonlinear model." << endl << 
        "Q. Exit." << endl << 
        "Choice operation: ";
        cin.ignore();
        cin.get(ch);
        cout << endl;
        switch(ch){
            case '1':{
                ClearScreen();
                _linear_obj -> printmodel();
                break;
            }
            case '2':{
                ClearScreen();
                _non_linear_obj -> printmodel();
                break;
            }
            case '3':{
                newheatvalue(_linear_obj);
                break;
            }
            case '4':{
                newheatvalue(_non_linear_obj);
                break;
            }
            case '5':{
                newoutputvalue(_linear_obj);
                break;
            }
            case '6':{
                newoutputvalue(_non_linear_obj);
                break;
            }
            case 'Q':
            case 'q':{
                break;
            }
            default:{
                ClearScreen();
                cout << "Uncorrected input!" << endl;
                break;
            }
        }
    }
}
void newheatvalue(TemperatureModel *_changed_value){
    ClearScreen();
    cout << "Current value: " << _changed_value -> getstartvalue() << endl << 
    "Enter the new value of the heat: ";
    
    double temp_value;

    cin >> temp_value;
    _changed_value -> changestartvalue(temp_value);
    cout << "Done!" << endl;
}
void newoutputvalue(TemperatureModel *_changed_value){
    ClearScreen();
    cout << "Current value: " << _changed_value -> getcalcnumber() << endl << 
    "Enter the new number of output values: ";

    unsigned int temp_value;

    cin >> temp_value;
    _changed_value -> changecalcnumber(temp_value);
    cout << "Done!" << endl;
}