///@file
///@Author Svetlana Lepshaya
///@22.12.2016
#include "lineinayaModel.h"
#include "neLineinayaModel.h"
#include <iostream>
#include <fstream>

using namespace std;

///@param parameters for PID-controller
	double T0 = 0.4, Td = 0.01, T = 0.85, K = 0.5, q0, q1, q2, e1 = 0, e2 = 0, e3 = 0, u = 0, w = 20, y = 0;
///@mainpage Model of Temperature 
///@image html C:\lin+nelin.png
///@brief We create objects and display them on the screen
int main()
{
	setlocale(0, "Russian");
	lineinayaModel lin;
	neLineinayaModel nelin;
	lin.show();
	cout << endl << endl;
	nelin.show();
	system("pause");
	return 0;
};