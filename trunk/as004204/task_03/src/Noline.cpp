#include "Noline.h"
#include <iostream>
#include <math.h>

using namespace::std;

///@brief Noline method solution 
void Noline::ShowResult() {
	for (int i = 1; i <= 10; i++)
	{
		if (i > 1)
		{
			array_y[i] = 0.9*array_y[i - 1] - 0.001*array_y[i - 2] * array_y[i - 2] + ut + sin(ut);
		}
	}

	///@brief Noline output 
	cout << "Don't liner solution: " << endl;
	for (int i = 1; i <= 10; i++)
	{
		cout << "t=" << i << " y=" << array_y[i] << endl;
	}
}