///@File
///@Author Vasiliuk Kirill
///@09.12.2016
#include "Line.h"
#include "Noline.h"
#include <iostream>

using namespace::std;
///@param Input temperature 
const double ut = 7;

///@mainpage Graphs of Temperature 
///@image html Line.png
///@image html Noline.png
int main()
{
	Line *ObjectLine = new Line;
	Noline *ObjectNoline = new Noline;
	ObjectLine->ShowResult();
	ObjectNoline->ShowResult();
	system("pause");
	return 0;
}