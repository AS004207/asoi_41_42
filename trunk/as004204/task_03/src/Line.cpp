#include "Line.h"
#include <iostream>

using namespace::std;

///@brief Line method solution 
void Line::ShowResult() {
	for (int i = 1; i <= 10; i++)
	{
		array_y[i] = 0.988*array_y[i - 1] + 0.232*ut;
	}
	///@brief Line output 
	cout << "Line solution: " << endl;
	for (int i = 1; i <= 10; i++)
	{
		cout << "t=" << i << " y=" << array_y[i] << endl;
	}
}