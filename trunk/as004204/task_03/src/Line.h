#pragma once
#include "Abstract.h"
using namespace::std;

///@class Line
///@brief Class Line derived from Abstract 
class Line :public Abstact
{
private:
	double array_y[11];
public:
	Line() :array_y() {}

	///@brief Line method solution 
	void ShowResult();
};